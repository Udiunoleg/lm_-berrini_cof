package map;

import java.util.Random;

import controllers.ControlledScanner;

public class Bonus {
	
	protected int bassistant;
	protected int bcoin;
	protected int bvpoint;
	protected int bpolicard;
	protected boolean bmainaction;
	protected int bnobility;
	
	public Bonus(){
		bassistant = 0;
		bcoin = 0;
		bvpoint = 0;
		bpolicard = 0;
		bmainaction = false;
		bnobility = 0;
	}
	
	public Bonus(int ass, int coin, int vpoint, int policard, boolean mainaction, int nobility){
		bassistant= ass;
		bcoin= coin;
		bvpoint = vpoint;
		bpolicard = policard;
		bmainaction = mainaction;
		bnobility = nobility;
	}
	
	/**
	 * shows the bonus available in the considered card\city\square (only if != 0)
	 */
	public void showBonus(){
		if(bassistant != 0)
			System.out.print("Assistants: " + bassistant + ", ");
		if(bcoin!= 0)
			System.out.print("Coins: " + bcoin + ", ");
		if(bvpoint != 0)
			System.out.print("Victory Points: " + bvpoint + ", ");
		if(bpolicard != 0)
			System.out.print("Politics Cards: " + bpolicard + ", ");
		if(bmainaction)
			System.out.print("Another MAIN ACTION, ");
		if(bnobility != 0)
			System.out.print("Nobility Points: " + bnobility);
		System.out.println();
	}
	
	/**
	 * checks if there is a particular type of bonus
	 */
	public boolean checkBonus(){
		if(bassistant!=0 || bcoin !=0 || bvpoint !=0 || bpolicard !=0 || bmainaction || bnobility !=0)
			return true;
		return false;
	}
	
	/**
	 * adds assistants/coins/victory points/nobility points to the hand of the Player p 
	 * or draw politics card or set another main action to the Player 
	 * @param p
	 */
	 public void useBonus(Player p){
		 	if(bassistant != 0){
		 		System.out.println("Asssistants: + " + bassistant);
				p.addAsssistants(bassistant);
			}
			if(bcoin!= 0){	
				System.out.println("Coins: + " + bcoin);
				p.addCoins(bcoin);
			}
			if(bvpoint != 0){
				System.out.println("Victory Points: + " + bvpoint);
				p.addVPoints(bvpoint);
			}
			if(bpolicard != 0){
				System.out.println("Politics cards drown: " + bpolicard);
				p.setDrawFlag(bpolicard);
			}
			if(bmainaction){
				System.out.println("Another Main Action Available");
				p.setAnotherMainAction(true);
			}
			if(bnobility != 0){
				System.out.println("Nobility Points: + " + bnobility);
				p.addNobilityPoints(bnobility);
			}
	 }
	 
	 /**
	  * checks the presence of a nobility point Bonus in the wanted card or city
	  * @return true if it's not present, false it there is at least a nobility point
	  */
	 public boolean notNobilityBonus(){
		 if(bnobility == 0)
			 return true;
		 else
			 return false;
	 }
	 
	 /**
	  * distributes bonus in the Permit Tiles relying on the number of cities present on them
	  * @param n (number of cities in a tile)
	  */
	 public void setRandomBonus(int n){
		 Random rand = new Random();
		 int numc;
		 switch(n){
		 case 1: numc = 3; 
		 		break;
		 case 2: numc = 2; 	
		 		break;
		 case 3: numc = 1; 
		 		break;
		 default: numc = 2;
		 		break;
		 }
		 int step1;
		 int step2;
		 int bass= 0;
		 int bcoi = 0;
		 int vp = 0;
		 int bpoli = 0;
		 boolean bmain = false;
		 int bnob = 0;
		 //50/50 between one or two bonus
		 step1 = rand.nextInt(2);
		 if(step1 == 0){
			 step2 = rand.nextInt(6);
			 //Randomly choose the bonus type
			 switch(step2){
			 //Assistants
			 case 0:
				 bass = (rand.nextInt(2)) + numc;
				 break;
			 //Coins
			 case 1:
				 bcoi = (rand.nextInt(2)) + (numc + 2);
				 break;
			 //Victory Points
			 case 2:
				 vp = (rand.nextInt(2)) + (numc + 2);
				 break;
			 //Policards
			 case 3:
				 bpoli = (rand.nextInt(2)) + (numc);
				 break;
			 //Main Action
			 case 4:
				 bmain = true;
				 break;
			 //Nobility Points
			 case 5:
				 bnob = (rand.nextInt(2)) + (numc -1);
				 break;
			 default: break;
			 }//end switch
			 
			 bassistant = bass;
			 bcoin = bcoi;
			 bvpoint = vp;
			 bpolicard = bpoli;
			 bmainaction = bmain;
			 bnobility = bnob;
			 return;
		//end if
		 }else{ //here step1 is "1"
			 int step2old= 9; //it must not be a number between 0 and 5 (9 is random)
			 for(step1 = 0; step1 < 2; step1++ ){
				//the two bonus must be different
			do{
			 step2 = rand.nextInt(6);
			}while(step2 == step2old);
			
			 //Randomly choose the bonus type
			 switch(step2){
			 //Assistants
			 case 0:
				 bass = numc;
				 break;
			 //Coin
			 case 1:
				 bcoi = (rand.nextInt(2)) + (numc);
				 break;
			 //Victory Points
			 case 2:
				 vp = (rand.nextInt(2)) + (numc);
				 break;
			 //Policards
			 case 3:
				 bpoli = numc;
				 break;
			 //Main Action
			 case 4:
				 if(numc != 1){
				 bmain = true;
				 }
				 break;
			 //Nobility Points
			 case 5:
				 bnob = 1;
				 break;
			 default: break;	 
			 }//end switch
			 step2old = step2;
			 }// end for
			 
			 bassistant = bass;
			 bcoin = bcoi;
			 bvpoint = vp;
			 bpolicard = bpoli;
			 bmainaction = bmain;
			 bnobility = bnob;
			 return ;
		 }// else end
	 }// THE END

	 /**
	  * creates a copy of that Bonus received at the input as a parameter
	  * @param bo
	  */
	 public void cloneBonus(Bonus bo){
		 bassistant = bo.bassistant; 
		 bcoin = bo.bcoin; 
		 bvpoint = bo.bvpoint; 
		 bpolicard = bo.bpolicard;
		 bmainaction = bo.bmainaction;
		 bnobility = bo.bnobility;
	}
	 
	 public int getBAssistantNumber(){return bassistant;}
	 public int getBCoinNumber(){return bcoin;}
	 public int getBVPointNumber(){return bvpoint;}
	 public int getBPolicardNumber(){return bpolicard;}
	 public boolean getBMainaction(){return bmainaction;}
	 public int getBNobilityNumber(){return bnobility;}
	 public void setAssistants(int num){bassistant = num;}
	 public void setCoins(int num){bcoin = num;}
	 public void setVPoint(int num){bvpoint = num;}
	 public void setPolicard(int num){bpolicard = num;}
	 public void setNobility(int num){bnobility = num;}
	 
	 /**
	  * makes Main Action available if the parameter num is 0, unavailable if it's different
	  * @param num
	  */
	 public void setMainAction(int num){
		 if(num == 0)
		 bmainaction = true;
		 else
		 bmainaction = false;
		 }
	 
	 /**
	  * asks Player which Bonus he wants to set in a Tile or City or a NobilitySquare
	  * in order to create customizable ones
	  */
		public void changeBonus(){
			ControlledScanner in = new ControlledScanner();
			int cho = -1;
			//ASSISTANTS
			System.out.println("Choose number of Assistants:");
			do{
				cho = in.nextInt();
			}while(cho < 0);
			setAssistants(cho);
			//COINS
			cho = -1;
			System.out.println("Choose number of Coins:");
			do{
				cho = in.nextInt();
			}while(cho < 0);
			setCoins(cho);
			//POLICARDS
			cho = -1;
			System.out.println("Choose number of Politics Cards:");
			do{
				cho = in.nextInt();
			}while(cho < 0);
			setPolicard(cho);
			//VPOINTS
			cho = -1;
			System.out.println("Choose number of Victory Points:");
			do{
				cho = in.nextInt();
			}while(cho < 0);
			setVPoint(cho);
			//NOBILITY POINTS
			cho = -1;
			System.out.println("Choose number of Nobility Points:");
			do{
				cho = in.nextInt();
			}while(cho < 0);
			setNobility(cho);
			//MAIN ACTION
			cho = -1;
			System.out.println("Do you want to set another Main Action:");
			System.out.println("0) Yes");
			System.out.println("1) No");
			do{
				cho = in.nextInt();
			}while(cho != 0 && cho != 1);
			setMainAction(cho);
		}
}
