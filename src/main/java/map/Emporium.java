package map;

public class Emporium {

	private City empcity;
	private Player play;
	
	public Emporium(City city, Player p){
		
		empcity = city;
		play= p;
	}
	public String empcityName(){return empcity.cityName();}
	public City getEmpoCity(){return empcity;}
	public String getEmpoCityname(){return empcity.cityName();}
	public Player getPlayer(){return play;}
}
