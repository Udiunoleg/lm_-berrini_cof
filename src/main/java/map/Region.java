package map;

import java.util.ArrayList;
import java.util.List;

import cards.Permitdeck;
import cards.Permittiles;
import controllers.ControlledScanner;
import council.Council;
import council.CouncillorPool;

public class Region {
	
	private String field;
	private Rewardsbonus regionbonus;
	private Council counc;
	private Permitdeck permdeck;
	private ArrayList<City> city;
	private int players;
	
	public Region(String fiel, CouncillorPool p, Constants co){
		regionbonus = new Rewardsbonus(0,0,5,0,false,0);
		counc = new Council(p);
		players = co.NUM_PLAYER;
		permdeck = new Permitdeck(this, players);
		city = new ArrayList<>();
		this.field = fiel;
	}
	
	public void createPermitDeck(){
		permdeck.createDeck(this);
	}
	
	public boolean hasRBonus(){
		if(regionbonus.isTaken())
			return false;
		else
			return true;
	}
	
	public void addCity(City c){
		city.add(c);
	}
	
	/** permits to choose a City giving a certain number in input
	 * @return a City (used in the Permit Tiles' personalization)
	 */
	public City getCity(){
		ControlledScanner in = new ControlledScanner();
		int num = 0;
		do{
			System.out.println("Select the city:");
			num = in.nextInt();
		}while(num > city.size());
		return city.get(num);
	}
	
	/** 
	 * @param name String that represents the name of the City
	 * @return a City object with nath name
	 */
	public City getCity(String name){
		for(int i=0; i < city.size(); i++){
			if(name.equals(city.get(i).cityName()))
				return city.get(i);
		}
		return new City(4);
	}
	
	public Council getCouncil(){return counc;}

	/**
	 * It shows the two up-sided permit tiles
	 */
	public void showShownPermitTiles(){
		if(field == "sea"){
			System.out.println("SEA TILES: ");
			for(int i = 0; i< (permdeck.shown).size(); i++){
				System.out.print(i + "°) ");
				showPermitTile((permdeck.shown).get(i));
			}
		}
		
		if(field == "hill"){
			System.out.println("HILL TILES: ");
			for(int i = 0; i< (permdeck.shown).size(); i++){
				System.out.print((i+2) + "°) ");
				showPermitTile((permdeck.shown).get(i));
			}
		}
		if(field == "mountain"){
			System.out.println("MOUNTAIN TILES: ");
			for(int i = 0; i< (permdeck.shown).size(); i++){
				System.out.print((i+4) + "°) ");
				showPermitTile((permdeck.shown).get(i));
			}
		}
	}
	
	public Permitdeck getPDeck(){return permdeck;}
	
	public void showRCouncil(){
		System.out.print(field + "'s council : ");
		counc.showCouncil();
	}
	
	public void showPermitTile(Permittiles card){
		List<Integer> index = card.getCity();
		ArrayList<City> tmp;
		tmp = intToCity(index);
		System.out.println(card.getRegion().getRegionName());
		System.out.print("Cities:");
		//It shows cities in this permit tile
		for(int i =0; i< tmp.size(); i++){
			System.out.print(" - ");
			System.out.print(tmp.get(i).cityName());
		}
		System.out.println();
		//It shows the bonus of this permit tile
		System.out.print("Bonus: ");
		card.getBonus().showBonus();
		if(card.isUsable() == false){
			System.out.println("USED");
		}
		System.out.println();
		return;
	}
	
	public void showCityPermitTile(Permittiles card){
		List<Integer> index = card.getCity();
		ArrayList<City> tmp;
		tmp = intToCity(index);
		System.out.print("Cities: ");
		//It shows cities in this permit tile
		for(int i =0; i< tmp.size(); i++){
			System.out.print("- ");
			System.out.print(tmp.get(i).cityName());
		}
		System.out.println();
		return;
	}
	
	//This method is for tests only
	public ArrayList<City> getAllCity(){return city;}
	public City getACity(int cityindex){return city.get(cityindex);}
	
	/**
	 * returns an Array of City from an Array of Integer (which represents cities's indexes in this region)
	 * @param cityint
	 */
	public ArrayList<City> intToCity(List<Integer> cityint){
		ArrayList<City> ct = new ArrayList<>();
		int tmp;
		for(int i =0 ; i < cityint.size(); i++){
			tmp = cityint.get(i);
			ct.add(city.get(tmp));
		}
		return ct;
	}
	
	public String getRegionName(){return field;}
	
	/**
	 * checks if a Player p has all the cities (it will be used for each region in method cityRewardsController)
	 * @param p
	 * @return true if it has, no if it hasn't
	 */
	public boolean allCityGot(Player p){
		for(int i =0; i < city.size(); i++){
			if(!(city.get(i).hasAnEmporium(p))){
				return false;
			}
		}
		return true;
	}
	
	public boolean isRewardTaken(){return  regionbonus.isTaken();}
	public Rewardsbonus getRewardBonus(){return regionbonus;}
	
	public boolean cityIsThere(String name){
		for(int i= 0; i < city.size() ; i++){
			if(city.get(i).cityName().equals(name)){
				return true;
			}
		}
		return false;
	}
	
	public City getCityByName(String namecity){
		for(int i= 0; i < city.size() ; i++){
			if(city.get(i).cityName().equals(namecity)){
				return city.get(i);
			}
		}
		return city.get(4);
	}
	
	public void showAllRegionCity(){
		for(int i=0; i < city.size(); i++){
			System.out.println(city.get(i).cityName());
		}
	}
	
	public void showAllCityBonusRegion(){
		for(int i=0; i < city.size(); i++){
			System.out.print(city.get(i).cityName() + " (" + city.get(i).getCityColor().getAbbreviation() +") "+ " -> ");
			if(!(city.get(i).cityName().equals("Juvelar")))
			city.get(i).showCityBonus();
			else
				System.out.println();
		}
	}
}