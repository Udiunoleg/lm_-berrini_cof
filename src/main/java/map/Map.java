package map;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import cards.Color;
import cards.Permittiles;
import cards.Politicsdeck;
import council.CouncillorPool;
import nobility.Nobilitytrack;

public class Map {

	private Region seaReg;
	private Region hillReg;
	private Region mountainReg;
	private Politicsdeck polideck;
	private CouncillorPool pool;
	private BonusCityPool bonuspool;
	public Constants cons;
	private ArrayList<Bonus> kingbonus;
	private Nobilitytrack nobilitytr;
	private King kg;
	
	public Rewardsbonus lightbluebonus;
	public Rewardsbonus graybonus;
	public Rewardsbonus orangebonus;
	public Rewardsbonus yellowbonus;

	
	public Map(String sea, String hill, String moun, Constants cos){
		pool = new CouncillorPool();
		cons = cos;
		seaReg = new Region("sea", pool, cons);
		hillReg = new Region("hill", pool, cons);
		mountainReg = new Region("mountain", pool, cons);
		polideck = new Politicsdeck();
		bonuspool = new BonusCityPool();
		kingbonus = new ArrayList<>();
		nobilitytr = new Nobilitytrack();
		kg = new King(pool);
		
		try {
			createMap(sea, hill, moun);
		} catch (IOException e) {
			Logger.getAnonymousLogger().log(Level.FINE, "context", e);
			System.out.println("File non trovato");
		}
		
		//Creates the 4 color bonus
		yellowbonus = new Rewardsbonus(0,0,2,0,false,0);
		graybonus = new Rewardsbonus(0,0,12,0,false,0);
		lightbluebonus = new Rewardsbonus(0,0,5,0,false,0);
		orangebonus = new Rewardsbonus(0,0,8,0,false,0);
		
		//Create 5 king bonus
		for(int t = 0 ; t < 5; t++){
			//1°
			kingbonus.add(new Bonus(0,0,25,0,false,0));
			//2°
			kingbonus.add(new Bonus(0,0,18,0,false,0));
			//3°
			kingbonus.add(new Bonus(0,0,12,0,false,0));
			//4°
			kingbonus.add(new Bonus(0,0,7,0,false,0));
			//5°
			kingbonus.add(new Bonus(0,0,3,0,false,0));
		}
	}

	/**
	 * Read map configuration from file and creates the gameboard
	 * @param sea name of the sea configuration (A or B)
	 * @param hill name of the hill configuration (A or B)
	 * @param mountain name of the mountain configuration (A or B)
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void createMap(String sea, String hill, String mountain) throws FileNotFoundException, IOException{
			FileReader filein = new FileReader("CityList.txt");	
			BufferedReader buf = new BufferedReader(filein);
			ArrayList<City> tmp = new ArrayList<>();
			String in;
			int num;
			City pt;
			Color col;
			//It creates an array list of 15 cities with no name
			for(int i=0; i < 15 ; i++){
				tmp.add(new City(cons.NUM_PLAYER));
			}
			//Now I link the cities of the sea region, I search the SEA A/B/C configuration
			do{
			in = buf.readLine();
			}while(!in.equals(sea));
			//Read all the SEA configuration until "end"
			while(!in.equals("end")){
				//Select the city in the array on "i" position
				for(int i=0; i < 5; i++){
					//I read the City name and save it in field "name" of the city
					in = buf.readLine();
					tmp.get(i).setName(in);
					//I read also the city color
					in = buf.readLine();
					col = takeColor(in);
					tmp.get(i).setColor(col);
					//Each city has 4 lines after the city's name
					for(int j=0; j<4 ; j++){
						in = buf.readLine();
						if(!in.equals("*")){
							num = Integer.parseInt(in);
							pt = tmp.get(num);
							tmp.get(i).addNearCity(pt);
						}
					}
				}
				in = buf.readLine();
			}

			//Same for Hill Region
			do{
				in = buf.readLine();
				}while(!in.equals(hill));
				//Read all the HILL configuration until "end"
			while(!in.equals("end")){
				//Select the city in the array on "i" position
				for(int i=5; i < 10; i++){
					//I read the City name and save it in field "name" of the city
					in = buf.readLine();
					tmp.get(i).setName(in);
					//I read also the city color
					in = buf.readLine();
					col = takeColor(in);
					tmp.get(i).setColor(col);
					//Each city has 4 lines after the city's name
					for(int j=0; j<4 ; j++){
						in = buf.readLine();
						if(!in.equals("*")){
							num = Integer.parseInt(in);
							pt = tmp.get(num);
							tmp.get(i).addNearCity(pt);
						}
					}
				}
				in = buf.readLine();
			}

				//Same for Mountain Region
			do{
				in = buf.readLine();
			}while(!in.equals(mountain));
			//Read all the MOUNTAIN configuration until "end"
			while(!in.equals("end")){
				//Select the city in the array on "i" position
				for(int i=10; i < 15; i++){
					//I read the City name and save it in field "name" of the city
					in = buf.readLine();
					tmp.get(i).setName(in);
					//I read also the city color
					in = buf.readLine();
					col = takeColor(in);
					tmp.get(i).setColor(col);
					//Each city has 4 lines after the city's name
					for(int j=0; j<4 ; j++){
						in = buf.readLine();
						if(!in.equals("*")){
							num = Integer.parseInt(in);
							pt = tmp.get(num);
							tmp.get(i).addNearCity(pt);
						}
					}
				}
				in = buf.readLine();
			}
			filein.close();
			distributeBonus(tmp);
			//Add 5 city from the array tmp to sea region (0 to 4)
			for(int i=0;i<5;i++){
				seaReg.addCity(tmp.get(i));
			}
			//Add 5 city from array tmp to hill region (5 to 9)
			for(int i=5; i<10; i++){
				hillReg.addCity(tmp.get(i));
			}
			//Add 5 city from array tmp to mountain region (10 to 15)
			for(int i=10; i < 15; i++){
				mountainReg.addCity(tmp.get(i));
			}
	}
	
	/**
	 * Converts a String to a color
	 * @param color's name
	 * @return a color
	 */
	public Color takeColor(String col){
				switch(col){
				case "Black": return Color.BLACK;
				case "LightBlue": return Color.LIGHTBLUE;
				case "Orange" : return  Color.ORANGE;
				case "Pink" : return Color.PINK;
				case "Violet": return Color.VIOLET;
				case "White" : return  Color.WHITE;
				case "Jolly":  return Color.JOLLY;
				case "Gray": return Color.GRAY;
				case "Yellow": return Color.YELLOW;
				default: return  Color.BLACK;
				}
		} 

	public Region getSeaR(){return seaReg;}
	public Region getHillR(){return hillReg;}
	public Region getMountR(){return mountainReg;}
	
	/**
	 * Gets a region from his name
	 * @param Region name
	 * @return Region Object
	 */
	public Region getRegion(String reg){
		switch(reg){
		case "sea": return seaReg;
		case "hill": return hillReg;
		case "mountain": return mountainReg;
		default: return seaReg;
		}
	}
	
	public King getKing(){return kg;}
	public Politicsdeck getPolideck(){return polideck;}
	public CouncillorPool getCouncillorPool(){return pool;}
	public Nobilitytrack getNobilitytrack(){return nobilitytr;}
	
	/**
	 * Checks if there are king's bonus
	 * @return true if are avaiable, false otherwise
	 */
	public boolean hasKingBonus(){
		if(kingbonus.isEmpty()){
			return false;
		}
		else{
			return true;
		}
	}
	
	public Bonus getKingbonus(){
			return kingbonus.get(0); 
		}
	
	public void removeKingBonus(){kingbonus.remove(0);}
			
	/*
	 * It takes each Bonus of the pool and assigns it to a city	
	 */
	public void distributeBonus(ArrayList<City> tmp2){
		for(int k=0; k < tmp2.size(); k++){
			if(k != 9){
			tmp2.get(k).setBonus(bonuspool.drawFirstBonus());
			bonuspool.removeFirstBonus();
			}
		}
		//Set the king on the city 9
		kg.move(tmp2.get(9));
	}
	
	public Bonus drawKingBonus(){
		Bonus btmp;
		btmp = kingbonus.get(0);
		kingbonus.remove(0);
		return btmp;
	}
	
	public void showAllCouncil(){
		seaReg.showRCouncil();
		hillReg.showRCouncil();
		mountainReg.showRCouncil();
		kg.showKingCouncil();
	}
	
	public void showKingCouncil(){kg.showKingCouncil();	}
	
	public void showAllShownPermitTiles(){
		seaReg.showShownPermitTiles();
		hillReg.showShownPermitTiles();
		mountainReg.showShownPermitTiles();
	}
	
	/**
	 * Shows all permit tile's cities and bonus
	 * @param card
	 */
	public void showPermitTile(Permittiles card){
		String reg;
		reg = card.getRegion().getRegionName();
		switch(reg){
		case "sea":{ 
			card.getRegion().showPermitTile(card);
			return;
		}
		case "hill":{
			card.getRegion().showPermitTile(card);
			return;
		}
		case "mountain":{
			card.getRegion().showPermitTile(card);
			return;
		}
		}
	}
	
	/**
	 * Shows all permit tile's cities
	 * @param card
	 */
	public void showCityPermitTile(Permittiles card){
		String reg;
		reg = card.getRegion().getRegionName();
		switch(reg){
		case "sea":{ 
			card.getRegion().showCityPermitTile(card);
			return;
		}
		case "hill":{
			card.getRegion().showCityPermitTile(card);
			return;
		}
		case "mountain":{
			card.getRegion().showCityPermitTile(card);
			return;
		}
		}
	}

	/**
	 * Converts a string city name to an Object city
	 * @param cityname String containing the name of the City
	 * @return city object
	 */
	public City nameToCity(String cityname){
		City cit;
		if(seaReg.cityIsThere(cityname)){
			cit = seaReg.getCityByName(cityname);
			return cit;
		}
		if(hillReg.cityIsThere(cityname)){
			cit = hillReg.getCityByName(cityname);
			return cit;
		}
		if(mountainReg.cityIsThere(cityname)){
			cit = mountainReg.getCityByName(cityname);
		return cit;
		}
		return null;
	}
	
	/**
	 * Shows all the city's names of the  map
	 */
	public void showAllCityName(){
		seaReg.showAllRegionCity();
		hillReg.showAllRegionCity();
		mountainReg.showAllRegionCity();
	}
	
	/**
	 * Shows all city bonus of all the regions
	 */
	public void showAllCityBonus(){
		seaReg.showAllCityBonusRegion();
		hillReg.showAllCityBonusRegion();
		mountainReg.showAllCityBonusRegion();
		System.out.println();
	}
	
	/**
	 * Checks if the city's name is in the map
	 * @param name for the  search
	 * @return true if a city's name is equals to String name, false otherwise
	 */
	public boolean cityIsThereMap(String name){
		if(seaReg.cityIsThere(name)){
			return true;
		}
		if(hillReg.cityIsThere(name)){
			return true;
		}
		if(mountainReg.cityIsThere(name)){
			return true;
		}
		return false;
	}
	
	/**
	 * Sets random emporia if it's started a game with only two players
	 */
	public void setRandomEmpo(){
		Permittiles t;
		List<Integer> num;
		ArrayList<City> cit;
		Player p = new Player(cons.NUM_EMPO, Color.JOLLY);
		//SEA
		t = getSeaR().getPDeck().getFirstCard();
		num = t.getCity();
		cit = getSeaR().intToCity(num);
		for(int i =0; i < cit.size(); i++){
			cit.get(i).buildEmpo(p);
			System.out.println("Emporium Built in " + cit.get(i).cityName() + " as initial Configuration");
		}
		getSeaR().getPDeck().discard(t);
		//HILL
		t = getHillR().getPDeck().getFirstCard();
		num = t.getCity();
		cit = getHillR().intToCity(num);
		for(int i =0; i < cit.size(); i++){
			cit.get(i).buildEmpo(p);
			System.out.println("Emporium Built in " + cit.get(i).cityName() + " as initial Configuration");
		}
		getHillR().getPDeck().discard(t);
		//MOUNTAINS
		t = getMountR().getPDeck().getFirstCard();
		num = t.getCity();
		cit = getMountR().intToCity(num);
		for(int i =0; i < cit.size(); i++){
			cit.get(i).buildEmpo(p);
			System.out.println("Emporium Built in " + cit.get(i).cityName() + " as initial Configuration");
		}
		getMountR().getPDeck().discard(t);
	}

}
