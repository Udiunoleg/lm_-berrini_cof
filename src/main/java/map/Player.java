package map;
import java.util.ArrayList;

import cards.Color;
import cards.Permittiles;
import cards.Politicscard;
import controllers.ControlledScanner;
import nobility.Marker;


public class Player {

	private Color pcolor;
	private Marker marker;
	private String playername;
	private ArrayList<Politicscard> policards;
	private ArrayList<Permittiles> ptiles;
	private int assistants;
	private ArrayList<Emporium> emporium;
	private int coins;
	private int victorypoints;
	boolean anotherMA;
	int drawFlag;
	private int maxempo;
	
	public Player(int NUM_EMPO, Color col){
		pcolor = col;
		policards = new ArrayList<>();
		ptiles = new ArrayList<>();
		emporium = new ArrayList<>();
		assistants= 0;
		coins= 0;
		victorypoints= 0;
		anotherMA = false;
		drawFlag = 0;
		maxempo = NUM_EMPO;
	}
	
	public void associateMarkerToPlayer(Marker m){
		marker = m;
	}
	
	public String getPlayerName() {return playername;}
	
	/**
	 * prints to insert a Player's Name at the beginning of a game
	 */
	public void setPlayerName(){
		ControlledScanner in = new ControlledScanner();
		do{
			System.out.println("Insert Player's Name:");
			playername = in.nextLine();
		}while(playername==null);
	}
	
	/**
	 * Sets Player Username with a String name
	 * @param name in input
	 */
	public void setPlayerName(String name){
		playername = name;
	}
	
	public Color getPlayerColor(){return  pcolor;}
	
	/**
	 * It checks in Player's emporium List if there's one with the same city name
	 * of the city where i'm trying to build an emporium
	 * @param city 
	 * @return true if it's present, false otherwise
	 */
	public boolean buildEmpo(City city){
		int i;
		for(i=0; i< emporium.size(); i++){
			if((emporium.get(i)).empcityName() == city.cityName()){
				return false;
			}		
		}
		emporium.add(new Emporium(city, this));
		return true;
	}
	
	/**
	 * checks if the Player already has an emporium on that City cit
	 * @param cit City where to check
	 * @return true if he hasn't, false if he already has
	 */
	public boolean checkEmpo(City cit){
		int i;
		for(i=0; i< emporium.size(); i++){
			if((emporium.get(i)).empcityName() == cit.cityName()){
				System.out.println("You have already an emporium here");
				return false;
			}
		}
		return true;
	}
	
	public int getCoins(){return coins;}
	
	public int getVpoints(){return victorypoints;}
	
	public int getAssistants(){return assistants;}
	
	/**
	 * Returns the number of politics card in hand
	 * 
	 */
	public int getHandSize(){return policards.size();}
	
	/**
	 * Checks if a Player has city of a certain color's number equals to num
	 * @param c Color to count
	 * @param num to compare
	 * @return tru if num is equals to the count of the color, false otherwise
	 */
	public boolean allCityOfAColor(Color c, int num){
		int count=0;
		for(int i =0; i < emporium.size(); i ++){
			if(emporium.get(i).getEmpoCity().getCityColor().equals(c)){
				count ++;
			}
		}
		if(count == num){
			return true;
		}else{
			return false;
		}
	}
	
	public ArrayList<Permittiles> getPermitTiles(){return ptiles;}
	
	public void addCoins(int con){
		coins = coins + con;
		System.out.println("Total Coins: "+ coins);
	}
	
	public void subCoins(int con){
		coins = coins - con;
		System.out.println("Total Coins: "+ coins);
	}
	
	public void addAsssistants(int ass){
		assistants = assistants + ass;
		System.out.println("Total Assistants: "+ assistants);
	}
	
	public void subAsssistants(int ass){
		assistants = assistants - ass;
		System.out.println("Total Assistants: "+ assistants);
	}
	
	public void addVPoints(int vpoint){
		victorypoints = victorypoints + vpoint;
		System.out.println("Total Victory Points: " + victorypoints);
	}
	
	public void addNobilityPoints(int nob){
		marker.move(nob);
	}
	
	public void addPoliCardInHand(Politicscard card){
		policards.add(card);
	}
	
	public void addPermitTileInHand(Permittiles tile){
		ptiles.add(tile);
	}
	
	public void setAnotherMainAction(boolean b){anotherMA = b;}
	public boolean getAMA(){return anotherMA;}
	public void setDrawFlag(int bo){ drawFlag = bo;}
	public int getDF(){return drawFlag;}
	public ArrayList<Politicscard> getHand(){return policards;}
	
	/**
	 * prints the politics cards available in the player's hand
	 */
	public void showPCard(){
		System.out.println();
		System.out.println("YOUR POLITICS CARDS: ");
		for(int i =0; i < policards.size() ; i++){
			policards.get(i).showCard();
		}
		System.out.println();
		System.out.println();
	}
	
	/**
	 * prints the number of Victory Points, Coins, Assistants of that Player and the position of the Marker in the Nobility Track
	 */
	public void showPoints(){
		System.out.println("YOUR POINTS:");
		System.out.println("Victory points: " + victorypoints);
		System.out.println("Coins: " + coins) ;
		System.out.println("Assistants: " + assistants);
		System.out.println("Marker Position: " + marker.getPosition());
		System.out.println();
	}
	
	/**
	 * shows the cities where the Player put an emporium in
	 */
	public void showEmporium(){
		for(int i = 0; i < emporium.size(); i++){
			System.out.print("Emporium " + i + " -> ");
			System.out.print(emporium.get(i).empcityName());
			emporium.get(i).getEmpoCity().showCityColor();
			System.out.println();
		}
		if(emporium.isEmpty())
			System.out.print("You haven't built in any City");
		System.out.println();
		System.out.println();
	}
	
	public void addPoliticscard(Politicscard pc){
		policards.add(pc);
	}
	
	public void addPermittile(Permittiles pt){
		pt.getBonus().useBonus(this);
		ptiles.add(pt);
	}
	
	/**
	 * Returns an ArrayList of the cities where the player built an emporium
	 */
	public ArrayList<City> builtCity(){
		ArrayList<City> bcity = new ArrayList<>();
		for(int i=0; i < emporium.size(); i++){
			bcity.add(emporium.get(i).getEmpoCity());
		}
		return bcity;
	}
	
	/**
	 * shows the Bonus of the Cities where a Player built an emporium
	 * @param bc List of the cities with a Player's emporium
	 */
	public void showBonusBuiltCity(ArrayList<City> bc){
		int i;
		for(i=0; i < bc.size(); i++){
			System.out.print(i + ") ");
			bc.get(i).showCityBonus();
		}
	}
	
	/**
	 * shows the Bonus of the Cities where a Player built an emporium without considering Nobility Points
	 * @param bc List of the cities with a Player's emporium
	 */
	public void showBBCWithoutNobility(ArrayList<City> bc){
		int i;
		for(i=0; i < bc.size(); i++){
			if(bc.get(i).getCityBonus().notNobilityBonus()){
				System.out.print(i + ") ");
				bc.get(i).showCityBonus();
			}
		}
	}
	
	/**
	 * Return how many politic cards of that color player have in hand
	 * @param c Color
	 */
	public int countColorInHand(Color c){
		int count = 0;
		for(int i=0; i < policards.size(); i++){
			if(policards.get(i).getColor()== c){
				count++;
			}
		}
		return count;
	}
	
	/**
	 * checks if a card of the color received in input is available in the Player's hand
	 * @return true if it's present, false otherwise
	 */
	public boolean checkColorInHand(Color c){
		for(int i =0; i < policards.size(); i++){
			if(policards.get(i).getColor()== c){
				return true;
			}
		}
		return false;
	}
			
	/**
	 * calculates if a Player wins
	 * @return true if a player won, false if nobody already won
	 */
	public boolean hasWon(){
		if(emporium.size() == maxempo){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Checks if all Permit Tiles in the player's hand are used
	 * @return true if all tiles are used, false otherwise
	 */
	public boolean allTilesUsed(){
		for(int i=0; i < ptiles.size(); i++){
			if(ptiles.get(i).isUsable()){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @return Player's Marker object
	 */
	public Marker getMarker(){return marker;}
}
