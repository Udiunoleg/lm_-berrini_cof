package map;

import java.util.ArrayList;

import council.Council;
import council.CouncillorPool;

public class King {

	private City currentcity;
	private Council kingcouncil;
	
	public King(CouncillorPool p){
		kingcouncil = new Council(p);
	}
	
	public void move(City nexthop){
			currentcity= nexthop;
	}
	
	
	public City whereIs(){return currentcity;}
	
	/**
	 * shows where is the king
	 */
	public void showWhereIs(){
		System.out.println("King is in " + currentcity.cityName() + " City");
		System.out.println();
	}
	
	/**
	 * shows the names of the cities near that of the king
	 */
	public void showNearCityNames(){
		System.out.println("King's near Cities:");
		for(int i =0; i < currentcity.getNearCities().size(); i++){
			System.out.print("- ");
			System.out.println(currentcity.getNearCities().get(i).cityName());
		}
	}
	
	/**
	 * @return names of the cities near the one in which there's the king
	 */
	public ArrayList<String> getNearCityName(){
		ArrayList<String> names = new ArrayList<>();
		for(int i=0; i < currentcity.getNearCities().size(); i++){
			names.add(currentcity.getNearCities().get(i).cityName());
		}
		return names;
	}
	
	/**
	 * shows King's Council
	 */
	public void showKingCouncil(){
		System.out.print("king's council : ");
		kingcouncil.showCouncil();
		System.out.println();
	}
	
	public Council getKingCouncil(){return kingcouncil;}
}