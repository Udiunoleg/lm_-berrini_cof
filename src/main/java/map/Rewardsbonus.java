package map;

public class Rewardsbonus extends Bonus {

	private Boolean taken;
	
	public Rewardsbonus(){}
	public Rewardsbonus( int ass, int coin, int vpoint, int policard, boolean mainaction, int nobility){
		
		bassistant= ass;
		bcoin= coin;
		bvpoint = vpoint;
		bmainaction = mainaction;
		bnobility = nobility;
		taken = false;
		
	}
	
	public boolean isTaken(){return taken;}
	public void setTaken(){ taken = true;}
}
