package map;

import java.util.ArrayList;
import java.util.Collections;

public class BonusCityPool {
	
	ArrayList<Bonus> pool;
	
	public BonusCityPool(){
		pool = new ArrayList<>();
		//14 Default Bonus
		//COINS
		pool.add(new Bonus(0,1,0,0,false,0));
		pool.add(new Bonus(0,2,0,0,false,0));
		pool.add(new Bonus(0,3,0,0,false,0));
		//NOBILITY
		pool.add(new Bonus(0,0,0,0,false,1));
		pool.add(new Bonus(0,0,0,0,false,1));
		//VICTORY POINT
		pool.add(new Bonus(0,0,1,0,false,0));
		pool.add(new Bonus(0,0,2,0,false,0));
		pool.add(new Bonus(0,0,3,0,false,0));
		//ASSISTANTS
		pool.add(new Bonus(1,0,0,0,false,0));
		pool.add(new Bonus(2,0,0,0,false,0));
		//POLITICS CARD
		pool.add(new Bonus(0,0,0,1,false,0));
		//MISC
		pool.add(new Bonus(0,0,1,1,false,0));
		pool.add(new Bonus(1,0,0,1,false,0));
		pool.add(new Bonus(1,1,0,0,false,0));
		shuffle();
		
	}
	public void shuffle(){
		Collections.shuffle(pool);
	}

	public ArrayList<Bonus> getArray(){return pool;}
	public Bonus drawFirstBonus(){return pool.get(0);}
	public void removeFirstBonus(){
		if(!pool.isEmpty()){
			pool.remove(0);
		}
	}
	
	public void printAllBonus(){
		for(int i =0; i < pool.size() ; i++){
			pool.get(i).showBonus();
		}
	}
}
