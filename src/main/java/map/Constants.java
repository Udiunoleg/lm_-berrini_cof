package map;

import java.util.concurrent.TimeUnit;

import controllers.ControlledScanner;

public class Constants {

	public int NUM_PLAYER= 4;
	public int NUM_EMPO= 10;
	public int TIMEOUT_TURN;
	public TimeUnit TIME_UNIT;
	
	public Constants(int nump, int numempo){
		NUM_PLAYER = nump;
		NUM_EMPO = numempo;
	}
	
	public Constants(){
		NUM_PLAYER = 4;
		NUM_EMPO = 10;
		TIMEOUT_TURN = 80000;
		TIME_UNIT = TimeUnit.SECONDS;
	}
	
	public void setPlayer(int num){NUM_PLAYER = num;}
	
	public void setPlayer(){
		ControlledScanner in = new ControlledScanner();
		NUM_PLAYER= in.nextInt();
	}
	public void setEmpo(int num){NUM_EMPO = num;}

	public void setEmpo(){
		ControlledScanner in = new ControlledScanner();
		NUM_EMPO = in.nextInt();
	}
}
