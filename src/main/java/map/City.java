package map;

import java.util.ArrayList;
import cards.Color;

public class City {
	
	private String name;
	private ArrayList<Emporium> emporium;
	private Bonus bonus;
	private Color citycolor;
	private ArrayList<City> nearcities;
	private Boolean king;
	int numplayer;
	
	public City(int NUM_PLAYER){
		emporium = new ArrayList<>();
		nearcities = new ArrayList<>();
		king = false;
		numplayer = NUM_PLAYER;
	}
	
	public int emporiumCount(){return emporium.size();}
	public String cityName(){return name;}
	
	public void setName(String n){
		name = n;
	}
	
	public void addNearCity(City pt){
		nearcities.add(pt);
	}
	
	public void setColor(Color col){citycolor = col;}
	public void setBonus(Bonus b){bonus = b;}
	public void showCityBonus(){bonus.showBonus();}
	public Bonus getCityBonus(){return bonus;}
	
	/**
	 * adds an Emporium to the Emporium's list of a Player
	 * @param p player
	 */
	public void buildEmpo(Player p){
		if(emporium.size() < numplayer)
			emporium.add(new Emporium(this, p));
		else{
			System.out.println("City FULL");
		}
	}
	
	public ArrayList<City> getNearCities(){return nearcities;}
	
	/**
	 * Checks if the Player has an emporium on the city
	 * @param p Player
	 * @return true if there's an emporium, false otherwise
	 */
	public boolean hasAnEmporium(Player p){
		for(int i =0; i < emporium.size(); i++){
			if(p.getPlayerColor().equals(emporium.get(i).getPlayer().getPlayerColor())){
				return  true;
			}
		}
		return false;
	}
	
	/**
	 * Clones a city object
	 * @param c City
	 */
	public void cloneCity(City c){
		name = c.name;
		emporium = c.emporium;
		bonus = c.bonus;
		citycolor = c.citycolor;
		nearcities = c.nearcities;
		king = c.king;
		numplayer = c.numplayer;
	}
	
	/**
	 * calls cityChangeBonus method and prints the City whose Bonus has been changed with the new Bonus
	 */
	public void cityChangeBonus(){
		bonus.changeBonus();
		System.out.println(name + " City's Bonus changed");
		System.out.println();
	}
	
	/**
	 * Prints on console the color of the city
	 */
	public void showCityColor(){
		System.out.print(" (" + citycolor.getAbbreviation() + ") ");
	}
	
	/**
	 * Returns the Color of the city
	 * @return
	 */
	public Color getCityColor(){return citycolor;}
}
