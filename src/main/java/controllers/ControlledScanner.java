package controllers;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControlledScanner {

	private Scanner scan;
	
	public ControlledScanner(){
		scan = new Scanner(System.in);
	}
	
	public int nextInt(){
		String str = "-2";
		int num = -2;
		int cont= 0;
		do{
			try{

				while(true){
					if(System.in.available()>0){
						str = scan.nextLine();
						break;
					}else{
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							Thread.currentThread().interrupt();
							throw new MyRuntimeException();
						}
					}
				}
				num = Integer.parseInt(str);
				return num;
			}catch(NumberFormatException | IOException e){	
				Logger.getAnonymousLogger().log(Level.FINE, "context", e);
				System.out.println("Invalid Input");
			}	
			cont++;
			if(cont >= 3){
				ayfkm();
			}
		}while(true);
	}
	
	public String nextLine(){
		String tmp;
		String tmp2;
		try {

			while(true){
				if(System.in.available()>0){
					tmp = scan.nextLine();
					break;
				}else{
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
						throw new MyRuntimeException();
						}
				}
			}
			tmp2 = upperCaseFirstLetter(tmp);
			if(tmp2.length() < 3){
				tmp = autocompleteCityName(tmp2);
				return tmp;
			}
			return tmp2;
		} catch (IOException e) {
			Logger.getAnonymousLogger().log(Level.FINE, "context", e);
			return "";
		}
	}
	
	/**
	 * transforms the first letter of a name as Capital letter
	 * @param n
	 * @return the same string as the input n with capital letter
	 */
	public String upperCaseFirstLetter(String n) {
		if(n.length()!=0){
			String iniziale = n.substring(0, 1);
			String finale = n.substring(1, n.length());
			return iniziale.toUpperCase() + finale.toLowerCase();
		}else{
			return "Unknown";
		}
	}
	
	/**
	 * prints a delightful meme
	 */
	public void ayfkm(){
		System.out.println("		░░░░░░░░▄▄██▀▀▀▀▀▀▀████▄▄▄▄░░░░░░░░░░░░░");
		System.out.println("		░░░░░▄██▀░░░░░░░░░░░░░░░░░▀▀██▄▄░░░░░░░░");
		System.out.println("		░░░░██░░░░░░░░░░░░░░░░░░░░░░░░▀▀█▄▄░░░░░");
		System.out.println("		░░▄█▀░░░░░░░░░░░░░░░░░░░░░░░░░░░░▀▀█▄░░░");
		System.out.println("		░▄█▀░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█▄░░");
		System.out.println("		░█▀░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▀█░");
		System.out.println("		▄█░░░░░░░░░░░░░░░░░░▄░░░░░░░░░░░░░░░░░██");
		System.out.println("		█▀░░░░░░██▄▄▄▄▄░░░░▄█░░░░░░░░░░░░░░░░░░█");
		System.out.println("		█░░░░░░░█▄░░▀██████▀░░░▄▄░░░░░░░░░░██░░█");
		System.out.println("		█░░░░░░░░▀█▄▄▄█▀░░░░░░░██▀▀██▄▄▄▄▄▄█░░▄█");
		System.out.println("		█░░░░░░░░░░░░░░░░░░░░░░░▀▄▄▄▀▀▀██▀░░░░█▀");
		System.out.println("		█▄░░░░░▄▄░░░░░░░░░░░░░░░░░░▀▀▀▀▀░░░░▄█▀░");
		System.out.println("		░█▄░░░░█░░░░▄▄▄▄░░░░░░░░░░░░░░░░░░░▄█░░░");
		System.out.println("		░░█▄░░▀█▄░░▀▀▀███████▄▄▄░░░▄░░░░░▄█▀░░░░");
		System.out.println("		░░░█▄░░░░░░░░░░░░░▀▀▀░░█░░░█░░░░██░░░░░░");
		System.out.println("		░░░░▀█▄▄░░░░░░░░░░░░░░░░░██░░░▄█▀░░░░░░░");
		System.out.println("		░░░░░░▀▀█▄▄▄░░░░░░░░░░░░░▄▄▄█▀▀░░░░░░░░░");
		System.out.println("		░░░░░░░░░░▀▀█▀▀███▄▄▄███▀▀▀░░░░░░░░░░░░░");
		System.out.println("		░░░░░░░░░░░█▀░░░░░░░░░░░░░░░░░░░░░░░░░░░");
		System.out.println("                      ARE YOU FU*KING KIDDING ME?");
	}
	
	/**
	 * permits to select a city giving in input only the first letter
	 * @param an input string
	 * @return city choosen name
	 */
	public String autocompleteCityName(String na){
		String iniziale = na.substring(0,1);
		switch(iniziale){
		case "A": return "Arkon";
		case "B": return "Burgen";
		case "C": return "Castrum";
		case "D": return "Dorful";
		case "E": return "Esti";
		case "F": return "Framek";
		case "G": return "Graden";
		case "H": return "Hellar";
		case "I": return "Indur";
		case "J": return "Juvelar";
		case "K": return "Kultos";
		case "L": return "Lyram";
		case "M": return "Merkatim";
		case "N": return "Naris";
		case "O": return "Osium";
		default: return "Nocity";
		}
	}
}
