package controllers;

import java.util.ArrayList;
import java.util.List;

import cards.Color;
import cards.Permittiles;
import council.Councillor;
import map.Bonus;
import map.City;
import map.Constants;
import map.Map;
import map.Player;
import nobility.Marker;

public class PlayerController {

	Player p;
	Map map;
	Marker mark;
	Market shop;
	Constants cos;
	Boolean mainaction;
	Boolean secondaryaction;
	Color playcolor;
	
	public PlayerController(Color col, Map m, Constants consta, Market mkt){
		cos = consta;
		playcolor= col;
		map = m;
		p= new Player(cos.NUM_EMPO, playcolor);
		mark =  new Marker(p , map, map.getNobilitytrack());
		p.associateMarkerToPlayer(mark);
		mainaction = true;
		secondaryaction = true;
		shop = mkt;
	}
	
	/**Initialize the number of Assistants and Coins for every Player
	 * just like the real game at the beginning of the match
	 * @param ass number of assistants
	 * @param co number of coins
	 */
	public void playerInitialize(int ass, int co){
		System.out.print(p.getPlayerName() + "'s ");
		p.addAsssistants(ass);
		System.out.print(p.getPlayerName() + "'s ");
		p.addCoins(co);
		//DRAW 6 CARDS
		drawPolicard();
		drawPolicard();
		drawPolicard();
		drawPolicard();
		drawPolicard();
		drawPolicard();
	}
	
	/**
	 * Starts a player's turn
	 */
	public void turn(){
		mainaction= true;
		p.setAnotherMainAction(false);
		secondaryaction = true;
		drawPolicard();
		while(mainaction||secondaryaction){
			selectAction();	
			if(checkAnotherMA()){
				mainaction = true;
				p.setAnotherMainAction(false);
			}
		}
		return;
	}
	
	/** 
	 * Starts a player's market turn to make a Offer (optional)
	 */
	public void marketTurnMakeOffer(){
		ControlledScanner in = new ControlledScanner();
		int answ = -1;
		do{
			System.out.println("Do you want to make an Offer?");
			System.out.println("1- YES");
			System.out.println("2- NO");
			answ = in.nextInt();
		}while(answ!=1 && answ != 2);
		
		if(answ == 2){//Do nothing
			return;
		}
		if(answ == 1){ // Make an Offer
			Offer deal = new Offer(p);
			deal.setAssistants(p.getAssistants());
			deal.setPCard(p);
			deal.setPTile(p);
			deal.setOfferPrice();
			shop.makeAnOffer(deal);
		}
	}
	
	/**
	 * Starts a Player's market turn to get a offer (optional)
	 */
	public void marketTurnGetOffer(){ // Get an Offer
		int offnumb;
		offnumb = shop.showAllOffer();
		System.out.println("YOUR COINS: " + p.getCoins());
		System.out.println(offnumb + ") End Turn");
		ControlledScanner in = new ControlledScanner();
		int offercost;
		int cho = -1;
		do{
			System.out.print("Select an offer or end your turn: ");
			cho = in.nextInt();
			if(cho == offnumb){//END TURN
				System.out.println();
				return;
			}
			offercost = shop.getAOffer(cho).getOfferCost();
		}while((cho < 0 || cho >= offnumb)||(offercost > p.getCoins()));
		
		//USE THE OFFER
		shop.getAOffer(cho).useOffer(p);
		shop.deleteOffer(cho);
		System.out.println();
		return;
	}
		
	/** 
	 * it makes the Player choose one of the Main Action
	 */
	public void mainAction(){
		//MAIN ACTION CONTROLLER
		if(!mainaction){
			System.out.println("You have already performed your Main Action");
			return;
		}
		int mact;
		
		do{
		//MAIN ACTION MENÙ INPUT
		System.out.println("Select your Main Action: ");
		System.out.println("1 - Build an emporium using a permit tile");
		System.out.println("2 - Build an emporium using the help of the king");
		System.out.println("3 - Elect a councillor (and gain 4 coins)");
		System.out.println("4 - Acquire a business permit tile");
		System.out.println();
		System.out.println("0 - Back");
		ControlledScanner in = new ControlledScanner();
		mact = (int) in.nextInt();
		}while(mact>4 && mact < 0);
		//MAIN  ACTION SWITCH CASE
		switch(mact){
			case 0: return;
			case 1: buildWithTile(); 
					return;
			case 2: buildWithKing(); 
					return;
			case 3: //CASE 3 - Elect a councillor --------------------------------------------
					electCouncillor();
					p.addCoins(4);
					mainaction = false;
					return;
			case 4: acquireATile(); 
					return;
			default: return;
		//SWITCH END
		}		
	}
	
	/**
	 * It builds an emporium using a permit tiles that you have
	 */
	public void buildWithTile(){
		//CASE 1 - Build an emporium using a permit tile -------------------------
		if(p.getPermitTiles().isEmpty()||p.allTilesUsed()){
			System.out.println("You haven't got any permit tile to use");
			return;
		}
		showUsablePermitTiles();
		int tile;
		Permittiles card;
		ControlledScanner num = new ControlledScanner();
		
		//Select a Tile in your Hand
		do{
			System.out.println("Select a Tile to build an emporium:");
			tile = num.nextInt();
		}while(tile > p.getPermitTiles().size() || tile < 0 || p.getPermitTiles().get(tile).isUsable() == false);
		card = p.getPermitTiles().get(tile);
		
		showCityPermitTile(tile);
		System.out.println();
		List<Integer> citytiles = card.getCity();
		
		//It prints indexes and names of the available cities
		for(int j = 0; j < citytiles.size(); j++){
			System.out.print(citytiles.get(j) + ") ");
			System.out.print(card.getRegion().getACity(citytiles.get(j)).cityName());
			card.getRegion().getACity(citytiles.get(j)).showCityColor();
			System.out.print(" -> ");
			if(!card.getRegion().getACity(citytiles.get(j)).cityName().equals("Juvelar")){
				card.getRegion().getACity(citytiles.get(j)).showCityBonus();
			}else{
				System.out.println();
			}
			}
		
		int city=0;
		
		//Select the city to build an emporium (City must be in the tile)
		do{
			System.out.println("Select the Number of the City in which you want to build an emporium:");
			city = num.nextInt();
		}while(cityInTile(citytiles, city)==false);
		
		City c = card.getRegion().getACity(city);
		
		if(buildController(c, card)){
			mainaction= false;
		}
	return;
	
	}
	
	/**
	 * It builds an emporium with the help of the King
	 */
	public void buildWithKing(){
		//CASE 2 - Build an emporium using the help of the king ------------------
		p.showPCard();
		map.getKing().showWhereIs();
		if(!(map.getKing().getKingCouncil().fulfillCouncil(this))){
			return;
		}
			
		ControlledScanner in = new ControlledScanner(); 
		int  choose;
		//Choose if build an emporium here or move the king
		do{
			do{
				System.out.println("Select an action:");
				if(p.checkEmpo(map.getKing().whereIs())){
					System.out.print("1) Build an emporium in "+ map.getKing().whereIs().cityName()+" city");
					map.getKing().whereIs().showCityColor();
					System.out.println();
				}
				if(p.getCoins() >= 2){
					System.out.println("2) Move the king in another city");
				}
				System.out.println("3) Back");
				choose = in.nextInt();
			}while(choose!= 1 && choose != 2 && choose != 3);
			
			if(choose == 1){ //BUILD HERE
				City tmp;
				tmp = map.getKing().whereIs();
				if(buildController(tmp)){
					return;
				}
			}
			
			if(choose == 2){
				if(p.getCoins()>= 2){ //MOVE THE KING		
					ArrayList<String> citynames = map.getKing().getNearCityName();
					String name;
					City tmp;
					//Select the city
					do{
						System.out.println("Select the city where you want to move the King:");
						map.getKing().showNearCityNames();		
						name= in.nextLine();		
					}while(!citynames.contains(name));
					
					//Move King to "tmp" city and sub 2 to Player's Coins
					tmp = map.nameToCity(name);
					map.getKing().move(tmp);
					p.subCoins(2);
				}
				if(p.getCoins()< 2){
					System.out.println("You haven't enough Coins");
				}
			}
			if(choose == 3){
				return;
			}
		}while(true);
	}
	
	/**
	 * It acquires a Tile from one council
	 */
	public void acquireATile(){
		//CASE 4 - Acquire a business permit tile ------------------------------
		map.showAllShownPermitTiles();
		map.showAllCouncil();
		p.showPCard();
		ControlledScanner in = new ControlledScanner();
		int coun;
		int tile  = 0;
		//Select a region Council to Fulfill
		do{ 
			System.out.println("Select the Council of a Region: ");
			System.out.println("1- Sea");
			System.out.println("2- Hill");
			System.out.println("3- Mountain");
			System.out.println();
			System.out.println("4- Back");
			coun = in.nextInt();
			if (coun > 4 || coun <= 0){
				System.out.println("Invalid Input");
				System.out.println();
			}
		}while(coun != 1 && coun != 2 && coun != 3 && coun!= 4);
		
		switch(coun){
		case 1:
			if(map.getSeaR().getCouncil().fulfillCouncil(this)){//council fulfilled
				do{
					System.out.println("Select Tile 0 or Tile 1: ");
					map.getSeaR().showShownPermitTiles();
				tile = in.nextInt();
				}while(tile != 0 && tile != 1);
				map.getSeaR().getPDeck().drawShowedTile(p, tile);
				mainaction= false;
			}
			return;		
		case 2:
			if(map.getHillR().getCouncil().fulfillCouncil(this)){
				do{
					System.out.println("Select Tile 2 or Tile 3: ");
					map.getHillR().showShownPermitTiles();
				tile = in.nextInt();
				}while(tile != 2 && tile != 3);
				tile = tile - 2;
				map.getHillR().getPDeck().drawShowedTile(p, tile);
				mainaction = false;
			}
			return;
		case 3:
			if(map.getMountR().getCouncil().fulfillCouncil(this)){
				do{
					System.out.println("Select Tile 4 or Tile 5: ");
					map.getMountR().showShownPermitTiles();
					tile = in.nextInt();
				}while(tile != 4 && tile != 5);
				tile = tile - 4;
				map.getMountR().getPDeck().drawShowedTile(p, tile);
				mainaction = false;
			}
			return;
		default: return;
		}//switch's end			
	
	}
	
	/**
	 * it makes the Player choose one of the Secondary Action
	 */
	public void secondaryAction(){
		if(!secondaryaction){
			System.out.println("You have already performed your Secondary Action");
			return;
		}
		int sact = 0;
		do{
			//SECONDARY ACTION MENÙ INPUT
			System.out.println("Select your Secondary Action: ");
			System.out.println("1 - Engage an Assisstant (for 3 coins)");
			System.out.println("2 - Change Business Permit Tiles in a Region (for an assistant)");
			System.out.println("3 - Send an assistant to elect a Councillor");
			System.out.println("4 - Perform an addictional Main Action (for 3 assistans)");
			System.out.println();
			System.out.println("0 - Back");
			ControlledScanner in = new ControlledScanner();
			sact = (int) in.nextInt();
			}while(sact>4 && sact < 0);
		switch(sact) {
		case 1:{// 1- Engage an Assisstant -------------------------------------------------
			if(p.getCoins() >= 3){
				p.subCoins(3);
				p.addAsssistants(1);
				secondaryaction = false;
				break;
			}else
				System.out.println("You haven't enough Coins");
			return;
		}
		case 2 : changeTileInRegion(); 
				 return;
		case 3:{// 3- Send an assistant to elect a Councillor ----------------------------
			if( p.getAssistants() >= 1){
				p.subAsssistants(1);
				electCouncillor();
				secondaryaction = false;
				return;
			}else{
				System.out.println("You haven't enough Assistants");
				return;
			}
		}
		case 4: performAnotherMA(); return;
		case 0: return;
		}//SWITCH  END
		
	//devo mettere a false secondaryaction	
	}
	
	/**
	 * Changes shown tiles in a region
	 */
	public void changeTileInRegion(){
		// 2- Change Business Permit Tiles in a Region ---------------------------
					if(p.getAssistants() == 0){
						System.out.println("You haven't enough Assistants");
						return;
					} else{
						p.subAsssistants(1);
						map.showAllShownPermitTiles();
						int reg;
						ControlledScanner in = new ControlledScanner();
						do{
							System.out.println("Select the Region in which you want to change Shown Tiles:");
							System.out.println("1- Sea");
							System.out.println("2- Hill");
							System.out.println("3- Mountain");
							reg = (int) in.nextInt();
						}while(reg!= 1 && reg != 2 && reg!= 3);
						
						switch(reg){
						case 1:
							map.getSeaR().getPDeck().changeShown();
							System.out.println("Sea Permit Tiles changed");
							System.out.println();
							map.getSeaR().showShownPermitTiles();
							break;
						
						case 2:
							map.getHillR().getPDeck().changeShown();
							System.out.println("Hill Permit Tiles changed");
							System.out.println();
							map.getHillR().showShownPermitTiles();
							break;
						
						case 3:
							map.getMountR().getPDeck().changeShown();
							System.out.println("Mountain Permit Tiles changed");
							System.out.println();
							map.getMountR().showShownPermitTiles();
							break;
							
						default: break;
						}//Switch end
						secondaryaction = false;
						return;
					}//else end	
	}
	
	/**
	 * Acquire another Main Action with 3 assistants
	 */
	public void performAnotherMA(){
		// 4- Perform an addictional Main Action ---------------------------------
					if((p.getAssistants() >= 3) && (mainaction == false)){
						p.subAsssistants(3);
						mainaction = true;
						secondaryaction = false;
						System.out.println("You can perform another Main Action");
						System.out.println();
						return;
					}else if(mainaction){
						System.out.println("You already have a Main Action to do");
						System.out.println();
						return;
					}
					System.out.println("You haven't enough Assistants");
					System.out.println();
					return;
	}
	
	/**
	 * Draws a policard from the deck
	 */
	public void drawPolicard(){
		map.getPolideck().draw(p);
	}
	
	/**
	 * it prints all the available actions to perform
	 */
	public void selectAction(){
		int act = 0;
		ControlledScanner in = new ControlledScanner();
		do{
			//if a bonus with another main action is used
			if(checkAnotherMA()){
				mainaction = true;
				p.setAnotherMainAction(false);
			}

			//If a bonus with policards is used
			if(p.getDF()!=0){
				for(int i=0; i<p.getDF(); i++){
					drawPolicard();
				}
				p.setDrawFlag(0);
			}
			
			System.out.println("////// Select an action: //////");
			//It shows only the actions that the player can do
			if(mainaction)
				System.out.println("0 - Main Action");
			if(secondaryaction)
				System.out.println("1 - Secondary Action");
			if(!mainaction)
				System.out.println("2 - End Turn");
			System.out.println("3 - Show Hand");
			System.out.println("4 - Show Points");
			System.out.println("5 - Show Emporium");
			System.out.println("6 - Show Permit Tiles");
			System.out.println("7 - Show City Bonus");

			act = (int) in.nextInt();
			switch(act){
				case 0:
					mainAction();
					return;
				case 1:
					secondaryAction();
					return;
				case 2: if(!mainaction){
							secondaryaction = false;
						}
						return;
				case 3: //show politics cards
					p.showPCard();
					return;
				case 4:
					p.showPoints();
					return;
				case 5:
					p.showEmporium();
					return;
				case 6://shows permit tiles
					showPermitTiles();
					return;
				case 7:
					map.showAllCityBonus();
					return;
			}
		}while(act < 0 || act > 7 );
	}
	
	/**
	 * it manages the election of the councillor in the selected Council
	 */
	public void electCouncillor(){
		int tm=0;
		ControlledScanner in = new ControlledScanner();
		map.showAllCouncil();
		Councillor c;
		c = map.getCouncillorPool().drawCouncillor();
		System.out.println("Select Council: ");
		System.out.println("1- Sea's Council");
		System.out.println("2- Hill's Council");
		System.out.println("3- Mountain's Council");
		System.out.println("4- King's Council");
		do{
			tm = in.nextInt();
		}while(tm != 1 && tm!= 2 && tm != 3 && tm != 4);
		switch(tm){
		case 1:
			map.getSeaR().getCouncil().addCouncillor(c);
			map.getSeaR().getCouncil().showCouncil();
			break;
		
		case 2:
			map.getHillR().getCouncil().addCouncillor(c);
			map.getHillR().getCouncil().showCouncil();
			break;

		case 3:
			map.getMountR().getCouncil().addCouncillor(c);
			map.getMountR().getCouncil().showCouncil();
			break;
		
		case 4:
			map.getKing().getKingCouncil().addCouncillor(c);
			map.getKing().getKingCouncil().showCouncil();
			break;
		}
		return;
	}
	
	/**
	 * it checks if another Main Action is available
	 * @return
	 */
	public boolean checkAnotherMA(){
		if (p.getAMA())
			return true;
		else
			return  false;
	}
	
	public Player getPlayer(){return this.p;}
	
	/**
	 * it takes the card from the hand and puts it in the deck (in a random position)
	 * @param c
	 */
	public void usePoliCard(Color c){
		for(int i = 0; i < p.getHand().size(); i++){
			if(p.getHand().get(i).getColor() == c){
				map.getPolideck().discard(p.getHand().get(i));
				p.getHand().remove(i);
				break;
			}
		}
	}
	
	/**
	 * it shows the Permit Tiles in the hand of a Player 
	 */
	public void showPermitTiles(){
		for(int i =0; i < p.getPermitTiles().size(); i++){
			System.out.print(i+ ") ");
			p.getPermitTiles().get(i).showTile();
		}
		if(p.getPermitTiles().size() == 0){
			System.out.println("You haven't got any Permit Tile");
			System.out.println();
		}
		return;
	}

	/**
	 * Shows all the cities on a permit tile
	 * @param the tile
	 */
	public void showCityPermitTile(int card){
		map.showCityPermitTile(p.getPermitTiles().get(card));
		return;
	}
	
	/**
	 * it shows the Permit Tiles which haven't been used yet in the hand of a Player 
	 */
	public void showUsablePermitTiles(){
		for(int i =0; i < p.getPermitTiles().size(); i++){
			if(p.getPermitTiles().get(i).isUsable() == true){
				System.out.print(i+ "-");
				map.showPermitTile(p.getPermitTiles().get(i));
			}
		}
		return;
	}
	
	/**
	 * checks if the number of the city is in the list of the cities
	 * @param list List of the cities as int
	 * @param city int that represents the city's number I want to check in the list
	 * @return true if it is, false if it isn't
	 */
	public boolean cityInTile(List<Integer> list, int city){
		for(int i=0; i < list.size(); i++){
			if(list.get(i) == city){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * It checks if a city name is in the list
	 * @param list
	 * @param cit
	 * @return true if the city is present, false otherwise
	 */
	public boolean alreadyUsed(ArrayList<String> list, City cit){
		for(int i =0; i < list.size(); i++){
			if(list.get(i).equals(cit.cityName())){
				return true;
			}
		}
		return  false;
	}
	
	/**
	 * Uses the bonus of the city and all the other linked cities which has already an emporium
	 * @param the city where I'm building
	 */
	public void useAllBonus(City c){
		ArrayList<String> taken = new ArrayList<>();
		taken.add(c.cityName());
		if(!c.cityName().equals("Juvelar")){
			c.getCityBonus().useBonus(p);
		}
		//Checks near cities if there's an emporium of the same player p
		for(int i=0; i < c.getNearCities().size(); i++){
			if((c.getNearCities().get(i).hasAnEmporium(p))&&(!alreadyUsed(taken, c))){
				c.getNearCities().get(i).getCityBonus().useBonus(p);
				taken.add(c.getNearCities().get(i).cityName());
				chainedBonus(taken, c.getNearCities().get(i));
			}
		}
	}
	
	/**
	 * Recorsive method used by useAllBonus()
	 * @param take is the ArrayList of the cities' names whose bonus were already used
	 * @param c the currrent city in exam
	 */
	public void chainedBonus(ArrayList<String> take, City c){
		for(int i=0; i < c.getNearCities().size(); i++){	
			if((c.getNearCities().get(i).hasAnEmporium(p))&&(!alreadyUsed(take, c))){
				c.getNearCities().get(i).getCityBonus().useBonus(p);
				take.add(c.getNearCities().get(i).cityName());
				chainedBonus(take, c.getNearCities().get(i));
			}
		}
	}
	
	/**
	 * it checks if the Player have enough assistants to built an Emporium in the given City c
	 * @param c
	 * @return true if he has, false if he hasn't
	 */
	public boolean payToBuild(City c){
		int bill;
		bill = c.emporiumCount();
		if(bill <= p.getAssistants()){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * It checks if a Player p built an emporium in all cities of the same region and gives him a Bonus (if available)
	 * @param p Player
	 */
	public void cityRewardsController(Player p){
		Bonus b;
		if((map.getSeaR().allCityGot(p))&&(map.getSeaR().isRewardTaken()==false)){
			System.out.println("You got all Cities of the Sea Region and You get the Region Bonus");
			map.getSeaR().getRewardBonus().useBonus(p);
			map.getSeaR().getRewardBonus().setTaken();
			if(map.hasKingBonus()){
				System.out.println("You get the King bonus");
				b= map.getKingbonus();
				b.useBonus(p);
				map.removeKingBonus();
			}else{
				System.out.println("No more King Bonus");
			}
			return;
		}
		if((map.getHillR().allCityGot(p))&&(map.getHillR().isRewardTaken()==false)){
			System.out.println("You got all Cities of the Hill Region and You get the Region Bonus");
			map.getHillR().getRewardBonus().useBonus(p);
			map.getHillR().getRewardBonus().setTaken();
			if(map.hasKingBonus()){
				System.out.println("You get the King bonus");
				b= map.getKingbonus();
				b.useBonus(p);
				map.removeKingBonus();
			}else{
				System.out.println("No more King Bonus");
			}
			return;
		}
		if((map.getMountR().allCityGot(p))&&(map.getMountR().isRewardTaken()==false)){
			System.out.println("You got all Cities of the Mountain Region and You get the Region Bonus");
			map.getMountR().getRewardBonus().useBonus(p);
			map.getMountR().getRewardBonus().setTaken();
			if(map.hasKingBonus()){
				System.out.println("You get the King bonus");
				b= map.getKingbonus();
				b.useBonus(p);
				map.removeKingBonus();
			}else{
				System.out.println("No more King Bonus");
			}
			return;
		}
	}
	
	/**
	 * Checks if a Player has all cities of some color and eventually gives him the bonus (if available)
	 * @param p Player
	 */
	public void cityColorRewardsController(Player p){
		Bonus b;
		int lightblue = 2;
		int orange = 3;
		int yellow = 5;
		int gray = 4;
		//LIGHTBLUE
		if(p.allCityOfAColor(Color.LIGHTBLUE, lightblue)){
			if(!map.lightbluebonus.isTaken()){
				System.out.println(p.getPlayerName() + " gets " + Color.LIGHTBLUE + " cities Bonus");
				map.lightbluebonus.useBonus(p);
				map.lightbluebonus.setTaken();
				if(map.hasKingBonus()){
					System.out.println("You get the King bonus");
					b= map.getKingbonus();
					b.useBonus(p);
					map.removeKingBonus();
				}else{
					System.out.println("No more King Bonus");
				}
			}else{
				System.out.println("Color Rewards already Taken");
			}
		}
		
		//ORANGE
		if(p.allCityOfAColor(Color.ORANGE, orange)){
			if(!map.orangebonus.isTaken()){
				System.out.println(p.getPlayerName() + "gets " + Color.ORANGE + " cities Bonus");
				map.orangebonus.useBonus(p);
				map.orangebonus.setTaken();
				if(map.hasKingBonus()){
					System.out.println("You get the King bonus");
					b= map.getKingbonus();
					b.useBonus(p);
					map.removeKingBonus();
				}else{
					System.out.println("No more King Bonus");
				}
			}else{
				System.out.println("Color Rewards already Taken");
			}
		}
		
		//GRAY
		if(p.allCityOfAColor(Color.GRAY, gray)){
			if(!map.graybonus.isTaken()){
				System.out.println(p.getPlayerName() + "gets " + Color.GRAY + " cities Bonus");
				map.graybonus.useBonus(p);
				map.graybonus.setTaken();
				if(map.hasKingBonus()){
					System.out.println("You get the King bonus");
					b= map.getKingbonus();
					b.useBonus(p);
					map.removeKingBonus();
				}else{
					System.out.println("No more King Bonus");
				}
			}else{
				System.out.println("Color Rewards already Taken");
			}
		}
		
		//YELLOW
		if(p.allCityOfAColor(Color.YELLOW, yellow)){
			if(!map.yellowbonus.isTaken()){
				System.out.println(p.getPlayerName() + "gets " + Color.YELLOW + " cities Bonus");
				map.yellowbonus.useBonus(p);
				map.yellowbonus.setTaken();
				if(map.hasKingBonus()){
					System.out.println("You get the King bonus");
					b= map.getKingbonus();
					b.useBonus(p);
					map.removeKingBonus();
				}else{
					System.out.println("No more King Bonus");
				}
			}else{
				System.out.println("Color Rewards already Taken");
			}
		}
		
	}
	
	/**
	 * Controls and eventually builds an emporium in City c
	 * @param c City
	 */
	public boolean buildController(City c){
		if(p.buildEmpo(c)&& payToBuild(c)){
			System.out.println("Emporium built in " + c.cityName());
			useAllBonus(c);
			mainaction = false;
			if(c.emporiumCount() > 0){
				System.out.println("-" + c.emporiumCount() + " Assistants because there are other "+ c.emporiumCount() + " emporium");
				p.subAsssistants(c.emporiumCount());
			}
			c.buildEmpo(p);
			cityRewardsController(p);
			cityColorRewardsController(p);
			return true;
		}else if(!(payToBuild(c))){
			System.out.println("Not enough assistants to build here");
			return false;
		}else{
			System.out.println("You already have an emporium here");
			return false;
		}
	}
	
	/**
	 * Build an Emporium (and checks if you haven't already builded an empo in the selected city)
	 * @param c Card
	 * @param card Permittiles
	 */
	public boolean buildController(City c, Permittiles card){
		if((p.buildEmpo(c))&&(payToBuild(c))){
			System.out.println("Emporium builded in " + c.cityName());
			useAllBonus(c);
			mainaction = false;
			card.usePermitTile();
			if(c.emporiumCount() > 0){
				System.out.println("-" + c.emporiumCount() + " Assistants because there are other "+ c.emporiumCount() + " emporium");
				p.subAsssistants(c.emporiumCount());
			}
			c.buildEmpo(p);
			cityRewardsController(p);
			cityColorRewardsController(p);
			return true;
		}else if(!(payToBuild(c))){
			System.out.println("Not enough assistants to build here");
			return false;
		}else{
			System.out.println("You already have an emporium here");
			return false;
		}
	}
}