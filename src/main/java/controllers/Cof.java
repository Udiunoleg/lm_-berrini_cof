package controllers;

import java.util.ArrayList;

import cards.Color;
import map.Constants;

public class Cof {

	public static void main(String[] args){
		System.out.println("∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞");
		System.out.println("∞∞∞∞∞∞∞∞∞∞∞ WELCOME TO COUNCIL OF FOUR ∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞");
		System.out.println("∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞");
		ControlledScanner in = new ControlledScanner();
		int numplay;
		do{
			System.out.println("Select number of Players: (2-8)");
			numplay = in.nextInt();
		}while(numplay < 2 || numplay > 8);
		
		ArrayList<String> name = new ArrayList<>();
		
		for(int i =0; i < numplay; i++){
			System.out.println("Player "+ i + " insert username:");
			name.add(in.nextLine());
			System.out.println("Player " + name.get(i) + " created ("+ Color.getColor(i).getAbbreviation()+" Player)");
			System.out.println();
		}
		
		Constants variables = new Constants();	
		variables.setPlayer(numplay);
				
		Game cof = new Game(variables, name);
		
		int answ= -1;
		do{
			System.out.println("Do you want to use the default configuration?");
			System.out.println("1 - YES");
			System.out.println("2 - NO");
			answ = in.nextInt();
		}while(answ != 1 && answ!= 2);

		if(answ == 1){  //DEFAULT PARAMETERS
			cof.play();
		}else{ //CUSTOM PARAMETERS
			
			int choice;
			
			do{
				System.out.println("Settings:");
				System.out.println("1) City Bonus");
				System.out.println("2) Nobility Track Bonus");
				System.out.println("3) Permit Tiles Bonus");
				System.out.println("-------------------------");
				System.out.println("0) Done!");
			
				do{//EDIT SETTINGS
					choice = in.nextInt();
				}while(choice < 0 || choice > 3);
				if(choice == 0){// 0) DONE!
					break;
				}
				cof.settingsSelection(choice);
			}while(true);
			
			cof.play();
			
		}
		return;
	}
	
}
