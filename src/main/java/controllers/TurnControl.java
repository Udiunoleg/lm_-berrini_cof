package controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import map.Constants;

public class TurnControl {

	PlayerController p;
	int timeout;
	int flag;
	
	public TurnControl(PlayerController play, Constants cos, int num){
		
		p = play;
		timeout = cos.TIMEOUT_TURN;
		flag= num;
	}
	
	public void turn(){ 

		ThreadTurn t = new ThreadTurn(p,flag);
		Thread timer = new Thread(){
			public void run(){
					try {
						Thread.sleep(timeout);

						  System.out.println();
						  System.out.println("****** Your turn's time is over *******");
						  System.out.println();
						  return;
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
						Logger.getAnonymousLogger().log(Level.FINE, "context", e);
						return;
					}
			}
		};
		timer.start();
		t.start();
		while(timer.isAlive()&& t.isAlive()){
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				Logger.getAnonymousLogger().log(Level.FINE, "context", e);
			}
		}
		timer.interrupt();
		t.interrupt();
		return;
	}
}
