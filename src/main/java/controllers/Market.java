package controllers;

import java.util.ArrayList;


public class Market {

	private ArrayList<Offer> off;
	
	public Market(){
		off = new ArrayList<>();
	}
	
	public int showAllOffer(){
		int number=0;
		for(int i=0; i < off.size(); i++){
			System.out.println("OFFER NUMBER " + i + ") ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			off.get(i).showAnOffer();
			System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			number++;
		}
		return number;
	}
	
	public void makeAnOffer(Offer o){off.add(o);}
	public Offer getAOffer(int index){return off.get(index);}
	public void deleteOffer(int num){ off.remove(num);}
	public void refoundUnselledOffers(){
		for(int i =0; i < off.size(); i++){
			off.get(i).refoundUnselledOffer();
			off.remove(i);
		}
	}
	/**
	 * Checks if Offer ArrayList is empty
	 * @return true if is empty, false otherwise
	 */
	public boolean isOffEmpty(){
		if(off.isEmpty()){
			return true;
		}else{
			return false;
		}
	}
	
}
