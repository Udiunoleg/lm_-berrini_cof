package controllers;

import java.util.ArrayList;
import java.util.Random;

import cards.Color;
import map.Constants;
import map.Map;

public class Game {
	
	private int numplayer;
	private PlayerController[] players;
	private Map gameboard;
	private Constants constant;
	private Market mark;
	private TurnControl t;
		
	public Game(Constants con, ArrayList<String> names){
		// The constuctor creates players
		mark = new Market();
		constant = con;
		numplayer = constant.NUM_PLAYER;
		players = new PlayerController[numplayer];
		chooseBoardsSide();
		for(int i = 0; i < numplayer; i++){
			players[i] = new PlayerController(Color.getColor(i), gameboard, constant, mark);
			players[i].getPlayer().setPlayerName(names.get(i));
		}
	}
	
	/**
	 * makes the game start after the initial configuration
	 */
	public void play(){
		boolean finished = false;
		int whofinishes = -1;
		playersInitialization(players);
		System.out.println();
		while(true){
		 	//Player's turn
			for(int i= 0; i < numplayer; i++){
				t = new TurnControl(players[i],constant,1);
				System.out.println("****** BEGIN "+ players[i].getPlayer().getPlayerName() +"'s TURN ****** (Player " + i + ")");
				t.turn();
				
				if(players[i].getPlayer().hasWon()){
					finished = true;
					players[i].getPlayer().addVPoints(3);
					whofinishes= i;
					System.out.println("*!*!*!*!*!* "+players[i].getPlayer().getPlayerName() + " HAS FINISHED HIS EMPORIA AND GETS 3 VICTORY POINTS *!*!*!*!*!*" );;
				}
			}
			//If someone has finished his emporium, The game skips the Market step
			if(finished){
				break;
			}
			//Player's Market Turn (to make offers)
			for(int i= 0; i < numplayer; i++){
				System.out.println("****** BEGIN " + players[i].getPlayer().getPlayerName()  + "'s MARKET TURN TO MAKE OFFERS*****");
				t = new TurnControl(players[i],constant,2);
				t.turn();

			}
			if(!mark.isOffEmpty()){
			//Player's Market Turn (to get offers)
				for(int i =0 ; i < numplayer; i++){
					System.out.println("****** BEGIN "+ players[i].getPlayer().getPlayerName() + "'s MARKET TURN TO GET OFFERS***** ");
					t = new TurnControl(players[i],constant,3);
					t.turn();
				}
				mark.refoundUnselledOffers();
			}else{
				System.out.println("***** NO ONE made an OFFER *****");
			}
		}
		//LAST TURNS of OTHER PLAYERS
		for(int i= 0; i < whofinishes; i++){
			System.out.println("****** BEGIN "+ players[i].getPlayer().getPlayerName() + "'s LAST TURN ******");
			players[i].turn();
		}
		endGame();
	}
	
	/**
	 * Calculate who is the winner on Victory points (and assistants, policard if needed )
	 */
	public void endGame(){
		int[] winner;
		whoIsTheMoreNobile();
		whoHasMorePermittiles();
		winner = whoHasWon();
		System.out.println("++++++++++ " + players[winner[0]].getPlayer().getPlayerName() + " HAS WON WITH "+ winner[1]+" VICTORY POINTS ++++++++++" );
		System.out.println();
		for(int i =0; i < players.length ; i++){
			System.out.println(players[i].getPlayer().getPlayerName() + " -> " + players[i].getPlayer().getVpoints() + " VP");
		}
		System.out.println();
		System.out.println("///////////////////////////////////////////////////////////////////////////////");
		System.out.println("/////////////////////////      GAME OVER            ///////////////////////////");
		System.out.println("///////////////////////////////////////////////////////////////////////////////");
	}
	
	/**
	 * Checks who has more permit tiles and gives him 3 VP
	 */
	public void whoHasMorePermittiles() {
		PlayerController top = players[0];
		int topnum=0;
		for(int i =0; i < players.length ; i++){
			if(topnum < players[i].getPlayer().getPermitTiles().size()){
				topnum = players[i].getPlayer().getPermitTiles().size();
				top = players[i];
			}
		}
		System.out.println(top.getPlayer().getPlayerName() + " gets 3 Victroy Points because He has more Permit Tiles");
		top.getPlayer().addVPoints(3);
	}

	/**
	 * Checks who is ahead in the nobility track and gives him 5 VP (2 to the second one)
	 */
	public void whoIsTheMoreNobile() {
		PlayerController top = players[0];
		PlayerController second = players[0];
		int topnum=0;
		int secondnum =0;
		for(int i =0; i < players.length; i++){
			if(topnum < players[i].getPlayer().getMarker().getPosition()){
				secondnum = topnum;
				second = top;
				topnum = players[i].getPlayer().getMarker().getPosition();
				top = players[i];
			}
		}
		System.out.println(top.getPlayer().getPlayerName() + " gets 5 Victory Points because He is FIRST on Nobility track (square " + topnum + ")");
		top.getPlayer().addVPoints(5);
		System.out.println(second.getPlayer().getPlayerName() + " gets 2 Victory Points because He is SECOND on Nobility track (square " + secondnum + ")");
		second.getPlayer().addVPoints(2);
	}
	
	/**
	 * Return the number of Assistants and Politics card which player has
	 * @param PlayerController one
	 * @return Assistant + Politics Card number
	 */
	public int totAssAndPCard(PlayerController one){
		int totOne = (one.getPlayer().getAssistants()) + (one.getPlayer().getHandSize());
			return totOne;
	}

	/**
	 * calculates the number of assistants and coins to set to every Player at the beginning of the turn
	 * @param Player Controller's array
	 */
	public void playersInitialization(PlayerController[] player){
		int ass= 1;
		int coin = 10;
		for(int i=0; i < numplayer; i++){
			player[i].playerInitialize(ass, coin);
			ass++;
			coin++;
		}
		if(player.length == 2){
			gameboard.setRandomEmpo();
		}
	}
	
	/** 
	 * @return the number of the Player who won the match and the number of his Victory points
	 */
	public int[] whoHasWon(){
		int[] winnerandpoints = new int[2];
		winnerandpoints[0] = -1; //PLAYER'S NUM
		winnerandpoints[1] = -1; //PLAYER'S POINTS
		int numAssPCard = -1;
		
		
		for(int i=0; i < numplayer; i++){
			
			if(players[i].getPlayer().getVpoints() == winnerandpoints[1]){
				
				if(numAssPCard < totAssAndPCard(players[i])){
					winnerandpoints[1] = players[i].getPlayer().getVpoints();
					winnerandpoints[0] = i;
					numAssPCard = totAssAndPCard(players[i]);
				}
			}
			if(players[i].getPlayer().getVpoints() > winnerandpoints[1]){
				winnerandpoints[1] = players[i].getPlayer().getVpoints();
				winnerandpoints[0] = i;
				numAssPCard = totAssAndPCard(players[i]);
			}
		}
		return winnerandpoints;
	}
	
	/**
	 * calls the methods for the object of the Game you wanted to customize 
	 * @param choices
	 */
	public void settingsSelection(int choices){
		switch(choices){
		case 1:
			cityBonusModifier();
			break;
		case 2:
			nobilityBonusModifier();
			break;
		case 3:
			permitTilesBonusModifier();
			break;
		case 0: break;
		default: break;
		}
	}
	
	/**
	 * gives the possibility to choose if you want to change the Bonus of a City
	 */
	public void cityBonusModifier(){
		ControlledScanner in = new ControlledScanner();
		String name;
		System.out.println("Select which City you want to change Bonus in: (type the initial of the City or the complete name, type 'Finish' to stop)");
		System.out.println();
		gameboard.showAllCityBonus();
		do{
			do{
				name = in.nextLine();
			}while((!gameboard.cityIsThereMap(name))&&(!name.equals("Finish")));
			
			if(name.equals("Finish")){
				return;
			}
			gameboard.nameToCity(name).cityChangeBonus();
			gameboard.showAllCityBonus();
		}while(true);
	}
	
	/**
	 * gives the possibility to choose if you want to change Bonus in a Nobilitysquare
	 */
	public void nobilityBonusModifier(){
		ControlledScanner in = new ControlledScanner();
		gameboard.getNobilitytrack().showNobilityTrack();
		System.out.println();
		int ind;
		do{
			System.out.println("Select which square you want to change Bonus in or press 0 to exit: (1 - 20)");
			do{
				ind = in.nextInt();
			}while(ind < 0 || ind > 20);
			if(ind == 0)
				return;
			gameboard.getNobilitytrack().getSquare(ind).nobilityChangeBonus(ind);
			gameboard.getNobilitytrack().showNobilityTrack();
		}while(true);
	}
	
	/**
	 * gives the possibility to choose if you want to change Bonus in a Permit Tile
	 */
	public void permitTilesBonusModifier(){
  		ControlledScanner in = new ControlledScanner();
		int pt;
		
		do{
			do{
				System.out.println("Select which Region you want to change Tiles in: (or press 0 to finish)");
				System.out.println("1) Sea ");
				System.out.println("2) Hill ");
				System.out.println("3) Mountain ");
				pt = in.nextInt();
			}while(pt<0 || pt>3);
			if(pt==0)
				return;
			regionPtBonusModifier(pt);
		}while(true);		
	}
	
	/**
	 * gives the possibility to choose which Permit Tiles you want to change Bonus in
	 * @param nreg int representing the number of the Region (1-sea, 2-hill, 3-mountain)
	 */
	public void regionPtBonusModifier(int nreg){
		ControlledScanner in = new ControlledScanner();
		System.out.println("Select which Permit Tiles you want to change Bonus in: (or -1 to Stop)");
		int pt;
		switch(nreg){
		
		case 1: 
			gameboard.getSeaR().getPDeck().showPdeck();
			if(numplayer < 5){
				do{
					pt = in.nextInt();
				}while(pt < -1 || pt > 13);
				if(pt == -1)
					return;
				if(pt>=0 && pt<13){
					gameboard.getSeaR().getPDeck().getPTile(pt).pTilesChangeBonus(pt);
					gameboard.getSeaR().getPDeck().showPdeck();
				}
			}else{
				do{
					pt = in.nextInt();
				}while(pt < -1 || pt > 28);
				if(pt == -1)
					return;
				if(pt>=0 && pt<28){
					gameboard.getSeaR().getPDeck().getPTile(pt).pTilesChangeBonus(pt);
					gameboard.getSeaR().getPDeck().showPdeck();
				}
			}
				break;
		case 2: 
			gameboard.getHillR().getPDeck().showPdeck();
			if(numplayer < 5){
				do{
					pt = in.nextInt();
				}while(pt < -1 || pt > 13);
				if(pt == -1)
					return;
				if(pt>=0 && pt<13){
					gameboard.getHillR().getPDeck().getPTile(pt).pTilesChangeBonus(pt);
					gameboard.getHillR().getPDeck().showPdeck();
			}
			}else{
				do{
					pt = in.nextInt();
				}while(pt < -1 || pt > 28);
				if(pt == -1)
					return;
				if(pt>=0 && pt<28){
					gameboard.getHillR().getPDeck().getPTile(pt).pTilesChangeBonus(pt);
					gameboard.getHillR().getPDeck().showPdeck();
				}
			}
				break;
		case 3: 
			gameboard.getMountR().getPDeck().showPdeck();
			if(numplayer < 5){
				do{
					pt = in.nextInt();
				}while(pt < -1 || pt > 13);
				if(pt == -1)
					return;
				if(pt>=0 && pt<13){
					gameboard.getMountR().getPDeck().getPTile(pt).pTilesChangeBonus(pt);
					gameboard.getMountR().getPDeck().showPdeck();
			}
			}else{
				do{
					pt = in.nextInt();
				}while(pt < -1 || pt > 28);
				if(pt == -1)
					return;
				if(pt>=0 && pt<28){
					gameboard.getMountR().getPDeck().getPTile(pt).pTilesChangeBonus(pt);
					gameboard.getMountR().getPDeck().showPdeck();
				}
			}
				break;
		default: return;
		}
	}
	
	/**
	 * gives the possibility to choose if you want a random map or you want to select the boards
	 */
	public void chooseBoardsSide(){
		String sea;
		String hill;
		String mount;
		int sean =0;;
		int hilln =0;;
		int mountn =0;;
		ControlledScanner in = new ControlledScanner();
		int choose= -1;
		do{
			System.out.println("Would you like to choose the 3 boards side or make it random?");
			System.out.println("1) Set Boards");
			System.out.println("2) Set it Random");
			choose = in.nextInt();
		}while(choose != 1 && choose != 2);
		
		if(choose == 1){ // CHOOSE MANUALLY-------------------------------------			
			//SEA BOARD
			do{ 
				System.out.println("Choose Sea Board side: ");
				System.out.println("1) Side A");
				System.out.println("2) Side B");
				sean = in.nextInt();
			}while(sean != 1 && sean != 2);

			//HILL BOARD
			do{ 
				System.out.println("Choose Hill Board side: ");
				System.out.println("1) Side A");
				System.out.println("2) Side B");
				hilln = in.nextInt();
			}while(hilln != 1 && hilln != 2);
						
			//MOUNTAIN BOARD
			do{ 
				System.out.println("Choose Mountain Board side: ");
				System.out.println("1) Side A");
				System.out.println("2) Side B");
				mountn = in.nextInt();
			}while(mountn != 1 && mountn != 2);
			
		}else{// GET RANDOM ---------------------------------------------------------------
			Random rand = new Random();
			sean = (rand.nextInt(2)+1);
			hilln = (rand.nextInt(2)+1);
			mountn = (rand.nextInt(2)+1);
		}
		//ASSIGNMENTS
		if(sean == 1){ 
			sea = "SEA A";
		}else{
			sea = "SEA B";
		}
		if(hilln == 1){
			hill = "HILL A";
		}else{
			hill = "HILL B";
		}
		if(mountn == 1){
			mount = "MOUNTAIN A";
		}else{
			mount = "MOUNTAIN B";
		}
		
		gameboard = new Map(sea, hill, mount, constant);
		System.out.println("Board Configuration: "+ sea +" - "+ hill +" - "+ mount);
	}
}
