package controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ThreadTurn extends Thread {

	int flag;
	PlayerController p;
	
	public ThreadTurn(PlayerController play, int f){
		flag = f;
		p = play;
	}
	
	@Override
	public void run() {

		switch(flag){
		case 1:
			try{
				p.turn();
			}catch(RuntimeException e){
				Logger.getAnonymousLogger().log(Level.FINE, "context", e);
			}
			break;
		case 2:
			try{
				p.marketTurnMakeOffer();
			}catch(RuntimeException e){
				Logger.getAnonymousLogger().log(Level.FINE, "context", e);
			}
			break;
		case 3:
			try{
				p.marketTurnGetOffer();
			}catch(RuntimeException e){
				Logger.getAnonymousLogger().log(Level.FINE, "context", e);
			}
			break;
		}
		return;
	}
}
