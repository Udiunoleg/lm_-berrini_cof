package controllers;

import java.util.ArrayList;
import cards.Permittiles;
import cards.Politicscard;
import map.Player;

public class Offer {
	
	private Player owner;
	private int assistants;
	private ArrayList<Politicscard> policard;
	private ArrayList<Permittiles> ptiles;
	private int price;
	
	public Offer(Player p){
		owner = p;
		policard = new ArrayList<>();
		ptiles = new ArrayList<>();
	}
	
	/**
	 * Shows all offers benefits and its cost
	 */
	public void showAnOffer(){
		System.out.println("Offer OWNER: "+ owner.getPlayerName() + " (" + owner.getPlayerColor().getAbbreviation()+ " Player)");
		System.out.println("Benefits:");
		
		//No print if value is 0 or NULL
		if(assistants != 0){
			System.out.println("Assistans: " + assistants);
		}

		if(!policard.isEmpty()){
			System.out.print("Politics cards: ");
			for(int i=0; i < policard.size(); i++){
				policard.get(i).showCard();
			}
		}
		if(!ptiles.isEmpty()){
			System.out.print("Permit Tiles: ");
			for(int i=0; i< ptiles.size(); i++){
				ptiles.get(i).showTile();
			}
		}
		System.out.println();
		System.out.println("Offer PRICE:" + price);
	}
	
	/**
	 * Sets a Offer's cost
	 */
	public void setOfferPrice(){
		ControlledScanner in = new ControlledScanner();
		int bill=0;
		do{
			System.out.print("How does it cost? : ");
			bill = in.nextInt();
		}while(bill < 0 || bill > 20);
		price = bill;
	}
	
	/**
	 * Sets number of assistants included in an offer
	 * @param maxass
	 */
	public void setAssistants(int maxass){
		ControlledScanner in = new ControlledScanner();
		int ass=0;
		do{
			System.out.print("How many Assistants would you like to offer? (You have " + owner.getAssistants()+  " assistants): ");
			ass = in.nextInt();
		}while(ass < 0 || ass > maxass);
		assistants = ass;
		owner.subAsssistants(ass);
	}
	
	/**
	 * sets number of Politics Cards included in an offer (make choosing from hand)
	 * @param The Player
	 */
	public void setPCard(Player p){
		ControlledScanner in = new ControlledScanner();
		int choose = -1;
		boolean stop = false;
		do{
			do{
				choose = -1;
				System.out.println("Select a Card to offer:");
				for(int i =0; i < p.getHand().size() ; i++){
					System.out.print(i + ") ");
					p.getHand().get(i).showCard();
					System.out.println();
				}
				System.out.println("-1) Stop");
				choose = in.nextInt();
				if(choose == -1){
					stop = true;
				}
			}while((choose < 0 || choose > p.getHandSize())&&(stop == false));
			if(stop == false){
			policard.add(new Politicscard(p.getHand().get(choose).getColor()));
			p.getHand().remove(choose);
			}
		}while(stop == false);
	}
	
	/**
	 * sets number of Politics Cards included in an offer (make choosing from hand)
	 * @param p
	 */
	public void setPTile(Player p){
		ControlledScanner in = new ControlledScanner();
		int choose = -1;
		boolean stop = false;
		do{
			do{
				choose = -1;
				System.out.println("Select a Tile to offer:");
				for(int i =0; i < p.getPermitTiles().size() ; i++){
					if(p.getPermitTiles().get(i).isUsable()){//si possono vedere solo quelle non gia usate
						System.out.print(i + ") ");
						p.getPermitTiles().get(i).showTile();
						System.out.println();
					}
				}
				System.out.println("-1) Stop");
				choose = in.nextInt();
				if(choose == -1){
					stop = true;
				}
				if(choose != -1 && !p.getPermitTiles().get(choose).isUsable()){
					System.out.println("You cannot offer an already used Tile");
					choose= -1;
				}
			}while((choose < 0 || choose > p.getPermitTiles().size())&&(stop == false));
			if(stop == false){
				ptiles.add(p.getPermitTiles().get(choose)); 
				p.getPermitTiles().remove(choose);
			}
		}while(stop == false);
	}
	
	/**
	 * Gets Politics cards from an offer
	 * @param buyer
	 */
	public void getPCards(Player buyer){// ADDS CARDS IN HAND
		for(int i=0; i < policard.size(); i++){
			Politicscard tmp = new Politicscard(policard.get(i).getColor());
			buyer.addPoliCardInHand(tmp);
		}
		policard.clear();
	}
	
	/**
	 * Gets tiles from an offer
	 * @param buyer
	 */
	public void getPTiles(Player buyer){
		for(int i = 0; i < ptiles.size(); i++){
			buyer.addPermitTileInHand(ptiles.get(i));
		}
		ptiles.clear();
	}
	
	public int getOfferCost(){return price;}
	
	/**
	 * Uses the offer's bonus
	 * @param buyer
	 */
	public void useOffer(Player buyer){
		
		buyer.subCoins(price);
	
		if(assistants != 0){//ADDS ASSISTANTS
			buyer.addAsssistants(assistants);
		}

		if(!policard.isEmpty()){
			getPCards(buyer);
		}
		if(!ptiles.isEmpty()){
			getPTiles(buyer);
		}
		System.out.println("Offer acquired for " + price+ " coins");	
	}
	
	/**
	 * Resets bonus of an unselled offer
	 */
	public void refoundUnselledOffer(){
		
		if(assistants != 0){//ADDS ASSISTANTS
			owner.addAsssistants(assistants);
		}

		if(!policard.isEmpty()){
			getPCards(owner);
		}
		if(!ptiles.isEmpty()){
			getPTiles(owner);
		}
	}
}
