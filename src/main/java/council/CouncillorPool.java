package council;

import java.util.ArrayList;

import cards.Color;
import controllers.ControlledScanner;

public class CouncillorPool {
	
	private ArrayList<Councillor> councilpool;
	
	public CouncillorPool(){
		councilpool = new ArrayList<>();
		int k;
		for(k=0; k<4; k++)
			councilpool.add(new Councillor(Color.BLACK));
		for(k=0; k<4; k++)
			councilpool.add(new Councillor(Color.LIGHTBLUE));
		for(k=0; k<4; k++)
			councilpool.add(new Councillor(Color.ORANGE));
		for(k=0; k<4; k++)
			councilpool.add(new Councillor(Color.PINK));
		for(k=0; k<4; k++)
			councilpool.add(new Councillor(Color.VIOLET));
		for(k=0; k<4; k++)
			councilpool.add(new Councillor(Color.WHITE));
	}
	
	/**
	 * @param c the color I want
	 * @return true if the councillor's color I want is available in the pool, false if it's not
	 */
	public boolean isFound(Color c){
		for(int i=0; i<councilpool.size(); i++){
			if(councilpool.get(i).getColor() == c) //get is an ArrayList's method
				return true;
		}
		return false;
	}
	
	/**
	 * shows the available colors of the councillors in the Pool
	 * @return List of available Colors
	 */
	public ArrayList<Color> showAvColors(){
		ArrayList<Color> available = new ArrayList<>();
		if(isFound(Color.BLACK))
			available.add(Color.BLACK);
		if(isFound(Color.LIGHTBLUE))
			available.add(Color.LIGHTBLUE);
		if(isFound(Color.ORANGE))
			available.add(Color.ORANGE);
		if(isFound(Color.PINK))
			available.add(Color.PINK);
		if(isFound(Color.VIOLET))
			available.add(Color.VIOLET);
		if(isFound(Color.WHITE))
			available.add(Color.WHITE);
		return available;
	}
	
	/**
	 * draws a random Councillor from the Councillor Pool 
	 * in order to add it to a Council of the game afterwards
	 * @return
	 */
	public Councillor drawCouncillor(){
		ArrayList<Color> av = new ArrayList<>();
		av = showAvColors();
		System.out.println("Select a councillor");
		for(int i=0; i < av.size(); i++)
			System.out.println(i + " - " +av.get(i).getAbbreviation());

		ControlledScanner con = new ControlledScanner();
		int num;
		do{
			num = con.nextInt();
			if(num<0 || num >=av.size()){
				System.out.println("Insert a valid number");
			}
		}while((num >= av.size()) || (num < 0));
		Color choosen = av.get(num);
		for(int j=0; j<councilpool.size(); j++){
			if(councilpool.get(j).getColor() == choosen){
				councilpool.remove(j);
				break;
			}
		}
		return new Councillor(choosen);
	}
	
	//overloading
	/**
	 * draws a Councillor of the chosen Color from the Councillor Pool
	 * @param c the Color that the Player wants
	 * @return a Councillor of the Color that the Player wants
	 */
	public Councillor drawCouncillor(Color c){
		Color choosen = c;
		for(int j=0; j<councilpool.size(); j++){
			if(councilpool.get(j).getColor() == choosen){
				councilpool.remove(j);
				break;
			}
		}
		return new Councillor(choosen);
	}
	
	public void addCouncillor(Councillor c){
		councilpool.add(c);
	}
	
	public int getSize(){
		return councilpool.size();
	}
}

	
