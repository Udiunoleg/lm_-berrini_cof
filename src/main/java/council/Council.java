package council;

import java.util.ArrayList;
import java.util.Random;

import cards.Color;
import controllers.ControlledScanner;
import controllers.PlayerController;

public class Council {
	private ArrayList<Councillor> balcony;
	private Random generator;
	private int num;
	private int nblack;
	private int nlightblue;
	private int norange;
	private int npink;
	private int nviolet;
	private int nwhite;
	private CouncillorPool pool;
	
	/** the constructor fills the balcony drawing councillors from the pool; 
	 * in the switch case, if there's no councillor
	 * of a certain color available, it continues in the next case
	 */
	public Council(CouncillorPool p){
		pool=p;
		balcony = new ArrayList<>(4);
		
		nblack=0;
		nlightblue=0;
		norange=0;
		npink=0;
		nviolet=0;
		nwhite=0;
		
		generator = new Random();
		
		for(int i=0; i<4;i++){
			num = generator.nextInt(5);
			switch (num){
				case 0:
					if(pool.isFound(Color.BLACK)){
						balcony.add(i, pool.drawCouncillor(Color.BLACK)); 
						break;
					}
				case 1:
					if(pool.isFound(Color.LIGHTBLUE)){
						balcony.add(i, pool.drawCouncillor(Color.LIGHTBLUE)); 
						break;
					}
				case 2:
					if(pool.isFound(Color.ORANGE)){
						balcony.add(i, pool.drawCouncillor(Color.ORANGE)); 
						break;
					}
				case 3:
					if(pool.isFound(Color.PINK)){
						balcony.add(i, pool.drawCouncillor(Color.PINK));
						break;
					}
				case 4:
					if(pool.isFound(Color.VIOLET)){
						balcony.add(i, pool.drawCouncillor(Color.VIOLET)); 
						break;
					}
				case 5:
					if(pool.isFound(Color.WHITE)){
						balcony.add(i, pool.drawCouncillor(Color.WHITE)); 
						break;
					}
				default: i--;
			}
		}
	}
	
	/**
	 * increase colors' number count checking a balcony
	 */
	public void getInfo(){
		nblack=0;
		nlightblue=0;
		norange=0;
		npink=0;
		nviolet=0;
		nwhite=0;
		for(int j=0; j<balcony.size(); j++){
			if(balcony.get(j).getColor() == Color.BLACK)
				nblack++;
			else if(balcony.get(j).getColor() == Color.LIGHTBLUE)
				nlightblue++;
			else if(balcony.get(j).getColor() == Color.ORANGE)
				norange++;
			else if(balcony.get(j).getColor() == Color.PINK)
				npink++;
			else if(balcony.get(j).getColor() == Color.VIOLET)
				nviolet++;
			else if(balcony.get(j).getColor() == Color.WHITE)
				nwhite++;
		}
	}
	
	/**
	 * print councillors' color in the council from the left to the right
	 */
	public void printOrder(){
		for(int k=0; k<4; k++){
			System.out.print(k+1 + "°) " + balcony.get(k).getColor() + "  ");
		}
	}
	
	/** add to the 0-position of the balcony the Councillor
	 * of the color I want and remove the one at the end 
	 */
	public void addCouncillor(Color c){
		balcony.add(0, pool.drawCouncillor(c));
		balcony.remove(4);
		getInfo();
	}
	
	/** add to the 0-position of the balcony one of the Councillor
	 * in the pool and remove the one at the end 
	 */
	public void addCouncillor(Councillor co){
		Councillor tmp;
		balcony.add(0, co);
		tmp = balcony.get(4);
		pool.addCouncillor(tmp);
		balcony.remove(4);
		//getInfo();
	}
	
	public void showCouncil(){
		printOrder();
		System.out.println();
	}
	
	/** Checks player's hand and returns how many cards he uses 
	 * in order to fulfill the chosen Council
	 */
	public boolean fulfillCouncil(PlayerController p){

		int cardused= 0;
		int col;
		int totcoin = p.getPlayer().getCoins();
		ArrayList<Integer> handcolor = new ArrayList<>();
		ArrayList<Integer> councilcolor = new ArrayList<>();
		
		getInfo();
		//Get how many councillors of each color council have
		councilcolor.add(0, nblack);
		councilcolor.add(1, nlightblue);
		councilcolor.add(2, norange);
		councilcolor.add(3, npink);
		councilcolor.add(4, nviolet);
		councilcolor.add(5, nwhite);

		//Get how many cards of each color player have
		handcolor.add(0, p.getPlayer().countColorInHand(Color.BLACK));
		handcolor.add(1, p.getPlayer().countColorInHand(Color.LIGHTBLUE));
		handcolor.add(2, p.getPlayer().countColorInHand(Color.ORANGE));
		handcolor.add(3, p.getPlayer().countColorInHand(Color.PINK));
		handcolor.add(4, p.getPlayer().countColorInHand(Color.VIOLET));
		handcolor.add(5, p.getPlayer().countColorInHand(Color.WHITE));
		handcolor.add(6, p.getPlayer().countColorInHand(Color.JOLLY));

		ArrayList<Integer> bestcard = bestOption(handcolor, councilcolor);
		if(totcoin < payForCouncil(bestcard.get(0), bestcard.get(1))){
			System.out.println("You haven't enough coins and cards to fulfill the council");
			return false;
		}
		System.out.println("Council consists of:");
		showCouncil();
		System.out.println();
		//Shows how many cards of each color player have
		System.out.println("You have these cards: ");
		if(handcolor.get(0) !=0)
			System.out.print("1)Black: "+ handcolor.get(0) +", ");
		if(handcolor.get(1) !=0)
			System.out.print("2)Light Blue: "+ handcolor.get(1) +", ");
		if(handcolor.get(2) !=0)
			System.out.print("3)Orange: "+ handcolor.get(2) +", ");
		if(handcolor.get(3) !=0)
			System.out.print("4)Pink: "+ handcolor.get(3) +", ");
		if(handcolor.get(4) !=0)
			System.out.print("5)Violet: "+ handcolor.get(4) +", ");
		if(handcolor.get(5) !=0)
			System.out.print("6)White: "+ handcolor.get(5) + ", ");
		if(handcolor.get(6) !=0)
			System.out.print("7)Jolly: "+ handcolor.get(6));
		System.out.println();
		System.out.println("0)Stop");
		System.out.println();
		ControlledScanner con = new ControlledScanner();
		
		//Select the color of the cards to use
		int oldnumcol;
		int oldnumhand;
		int i =0;
		int jolly = 0;
		int bill;
		int bestc = bestcard.get(0) + bestcard.get(1);
		//Each iteration select a card
		do{
			bill = 0;
			System.out.println("Select the " + (i+1) + "° card ");
			col = ((int) con.nextInt()) -1;
			
			//The STOP Clause
			if(col == -1 && cardused > 0){
				bill = payForCouncil(cardused, jolly);
				if(bill <= totcoin){
					p.getPlayer().subCoins(bill);
					System.out.println("Council Fulfilled for " + cardused + " cards " + "(" + jolly + " jolly) and " + bill + " coins" );
					System.out.println();
					return true;
				}
			}else{
				if(col != -1 ){
			oldnumcol=0;
			if(col != 6){//Get the color occurrence number in the hand and in the council
			oldnumcol = councilcolor.get(col);
			}
			oldnumhand = handcolor.get(col);

			if(oldnumcol > 0 || col == 6){//If the color is in the council or col is the Jolly
				
				if(oldnumhand > 0 && col != 6){//if you have that color in the hand
						p.usePoliCard(intToColor(col));
						oldnumhand--;
						handcolor.set(col, oldnumhand);
						cardused++;
						i++;
				}else{
					if(oldnumhand > 0 && col == 6){
							p.usePoliCard(intToColor(6));
							oldnumhand--;
							handcolor.set(col, oldnumhand);
							cardused++;
							jolly++;
							i++;
					}else{
					System.out.println("You haven't that Card");
				}
				}
				
			}else{
				System.out.println("That color is not in the council");
			}
			}else{
				System.out.println("You must use at least one card!");
			}
			}
		}while(cardused < bestc);
		bill = payForCouncil(cardused, jolly);
		p.getPlayer().subCoins(bill);
		System.out.println("Council Fulfilled for " + cardused + " cards " + "(" + jolly + " jolly) and " + bill + " coins" );
		System.out.println();
		return true;
	}
	
	/** 
	 * @param col int number
	 * @return Color object corresponding to col number
	 */
	public Color intToColor(int col){
		Color[] colors ={Color.BLACK, Color.LIGHTBLUE, Color.ORANGE, Color.PINK, Color.VIOLET, Color.WHITE, Color.YELLOW, Color.GRAY};
		if(-1<col&& col<8)
			return colors[col];
		else
			return Color.JOLLY;
		} 
	
	/** calculates the total amount of coins to pay in order to satisfy a Council 
	 * @param cards number of cards used
	 * @param jolly number of jolly cards used 
	 * @return
	 */
	public int payForCouncil(int cards, int jolly){
		int tot =0;
		tot = tot + jolly;
		switch(cards){
		case 1:
			tot= tot + 10;
			return tot;
		case 2:
			tot = tot + 7;
			return tot;
		case 3:
			tot = tot + 4;
			return tot;
		case 4:
			return tot;
		default: 
			return  tot;
		}
	}
	
	/**
	 * Calculate the "best option", that is the best combination of cards that a Player can use to satisfy a Council
	 * @param handcolor List of the cards the Player has in the hand
	 * @param councilcolor List of the Colors of the Councillors in a selected Council
	 * @return an arraylist of 2 Integer (first number normal cards, second number jolly)
	 */
	public ArrayList<Integer> bestOption(ArrayList<Integer> handcolor, ArrayList<Integer> councilcolor){
		int card = 0;
		int coun;
		int hand;
		ArrayList<Integer> c = new ArrayList<>(2);
		for(int i =0;i < councilcolor.size() && (card < 4); i++){
			coun = councilcolor.get(i);
			hand = handcolor.get(i);
			if(coun >0){
				if(coun >= hand){
				card = card + handcolor.get(i);
				}else{
					card = card + councilcolor.get(i);
				}
			}
		}
		if(card == 4){
			c.add(0, card);
			c.add(1, 0);
			return c;
		}
		if(handcolor.get(6) >= (4-card) ){
			card = card + (4-card);
			c.add(0, card);
			c.add(1, (4-card));	

		}else{
			card = card + handcolor.get(6);
			c.add(0, card);
			c.add(1, handcolor.get(6));
		}
		return c;
	}
}
