package council;

import cards.Color;

public class Councillor {
	private Color color;
	
	public Councillor(Color color){
		this.color = color;
	}
	
	public Color getColor() {return color;}
}
