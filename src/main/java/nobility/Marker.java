package nobility;

import cards.Permittiles;
import controllers.ControlledScanner;
import map.Map;
import map.Player;
import map.Bonus;
import map.City;
import java.util.ArrayList;


public class Marker {
	private int position; 
	private Nobilitytrack circuit;
	private Player p1;
	private Map m;
	
	public Marker(Player pla, Map mp, Nobilitytrack nt){
		position = 0;
		p1 = pla;
		m = mp;
		circuit = nt;
	}
	
	/**
	 * moves the Marker inside the NobilityTrack 
	 * @param num number of squares the marker will pass
	 */
	public void move(int num){
		for(int i=num; i>0; i--){
			position = position + 1;
			if(circuit.getSquare(position).getAltBonus() != 0){
				//manageAltBonus() method?
				int aus;
				aus=circuit.getSquare(position).getAltBonus();
				switch(aus){
				case 1: System.out.println("You reached the square " + position + " in Nobility Track and You can get the bonus:");
						getBuiltCityBonus(1);
						break;
						
				case 2:	System.out.println("You reached the square " + position + " in Nobility Track and You can get the bonus:");
						getFreePermittile();
						break;
						
				case 3: System.out.println("You reached the square " + position + " in Nobility Track and You can get the bonus:");
						getPermittileBonus();
						break;
						
				case 4: System.out.println("You reached the square " + position + " in Nobility Track and You can get the bonus:");
						getBuiltCityBonus(2);
						break;
				}
			}
			else if(circuit.getSquare(position).hasRealBonus()){
					System.out.println("You reached the square " + position + " in Nobility Track and You get the bonus:");
					circuit.getSquare(position).getBonus().useBonus(p1);
			}
		}
	}
	
	public int getPosition(){return position;}
	
	/**
	 * gets Bonus of 1\2 city where the player already built an emporium in
	 * @param numb 1 if the Bonus permits to choose a bonus, 2 if it permits to choose 2 bonus
	 */
	public void getBuiltCityBonus(int numb){
		ArrayList<City> c;
		Bonus bo;
		Bonus bn;
		ControlledScanner cs = new ControlledScanner();
		int j;
		int k;
		c = p1.builtCity();
		p1.showBBCWithoutNobility(c);
		if(!c.isEmpty()){
			if(numb==1){
				do{
					System.out.println("Choose one city's bonus: ");
					j = cs.nextInt();
				} while(j<0 || j > c.size());
				bo = c.get(j).getCityBonus();
				bo.useBonus(p1);
			}
			else if(numb==2){
				do{ System.out.println("Choose two city's bonus: ");
					j= cs.nextInt();
					k= cs.nextInt();
				} while(j<0 || j >c.size() || k<0 || k>c.size() || j==k);
				bo = c.get(j).getCityBonus();
				bo.useBonus(p1);
				bn = c.get(k).getCityBonus();
				bn.useBonus(p1);
			}
		}
		else{ 
			System.out.println("You can't choose a city's bonus since you didn't build in any city");
		}
	}
	
	/**
	 * receives a Permit Tile without satisfying a region's council
	 */
	public void getFreePermittile(){
		m.getSeaR().showShownPermitTiles();
		m.getHillR().showShownPermitTiles();
		m.getMountR().showShownPermitTiles();
		
		ControlledScanner con = new ControlledScanner();
		int k;
		do {
			System.out.println("Choose 1 permit tile: ");
			k = con.nextInt();
		} while(k<0 || k >5);
		
		switch(k){
			case 0: m.getSeaR().getPDeck().drawShowedTile(p1, k); 
					break;
			case 1: m.getSeaR().getPDeck().drawShowedTile(p1, k); 
					break;
			case 2: m.getHillR().getPDeck().drawShowedTile(p1, k-2); 
					break;
			case 3: m.getHillR().getPDeck().drawShowedTile(p1, k-2);
					break;
			case 4: m.getMountR().getPDeck().drawShowedTile(p1, k-4); 
					break;
			case 5: m.getMountR().getPDeck().drawShowedTile(p1, k-4); 
					break;
		}	
	}
	
	 /**
	  * receives a Bonus of a tile which the player already used or has currently in the hand
	  */
	public void getPermittileBonus(){
		ArrayList<Permittiles> perm;
		Bonus bu;
		int h;
		ControlledScanner in = new ControlledScanner();
		perm = p1.getPermitTiles();
		for(int i =0; i < perm.size(); i++){
			System.out.print(i + ") ");
			perm.get(i).getBonus().showBonus();
		}
		do {
			System.out.println("Choose the bonus of 1 Permittile: ");
			h=in.nextInt();
		} while(h<0 || h>perm.size());
		bu = perm.get(h).getBonus();
		bu.useBonus(p1);
	}
}
