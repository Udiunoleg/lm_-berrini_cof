package nobility;

import controllers.ControlledScanner;
import map.Bonus;

public class Nobilitysquare {
	private Bonus squarebonus;
	private boolean action1;
	private boolean action2;
	private boolean action3;
	private boolean action4;
	
	public Nobilitysquare(){
		squarebonus = new Bonus();
		action1 = false;
		action2 = false;
		action3 = false;
		action4 = false;
	}
	
	public Bonus getBonus(){return squarebonus;}
	
	public void setBonus(Bonus bsquare){
		squarebonus.cloneBonus(bsquare);;
	}
	
	/**
	 * checks what kind of Alternative Bonus there is in a NobilitySquare
	 * @return
	 */
	public int getAltBonus(){
		if(action1)
			return 1;
		if(action2)
			return 2;
		if(action3)
			return 3;
		if(action4)
			return 4;
		return 0;
	}
	
	/** 
	 * sets Alternative Bonus on NobilitySquare
	 * @param first 
	 * @param second
	 * @param third
	 * @param fourth
	 */
	public void setAltBonus(boolean first, boolean second, boolean third, boolean fourth){
		action1=first;
		action2=second;
		action3=third;
		action4=fourth;
	}
	
	/** checks if the Nobility Square has a standard Bonus
	 * @return true if yes, false if not
	 */
	public boolean hasRealBonus(){
		return squarebonus.checkBonus(); 
	}
	
	/**
	 * changes Alternative Bonus on the NobilitySquare
	 */
	public void changeAltBonus(){
		ControlledScanner cs = new ControlledScanner();
		int act;
		int choose;
		System.out.println("Do you want to set an additional bonus? (only if you didn't choose to set any other Bonus previously)");
		System.out.println("0) Yes");
		System.out.println("1) No");
		do{ 
			act=cs.nextInt();
		}while(act!=0 && act!=1);
		if(act==0){
			System.out.println("Which one?");
			System.out.println("0) Get a free bonus of a City which you've already built an emporium in (excluded cities with Nobility points)");
			System.out.println("1) Take a face up Permit Tile without paying the cost");
			System.out.println("2) Receive the bonus of a Permit Tile that you've already bought");
			System.out.println("3) Get 2 free bonus of 2 Cities which you've already built an emporium in (excluded cities with Nobility points)");
			do{
				choose=cs.nextInt();
			}while(choose!=0 && choose!=1 && choose!=2 && choose!=3);
			if(choose==0)
				setAltBonus(true, false, false, false);
			if(choose==1)
				setAltBonus(false, true, false, false);
			if(choose==2)
				setAltBonus(false, false, true, false);
			if(choose==3)
				setAltBonus(false, false, false, true);
		}
		else 
			setAltBonus(false, false, false, false);
	}
	
	/** changes the bonus on a selected NobilitySquare 
	 * @param ind
	 */
	public void nobilityChangeBonus(int ind){
		squarebonus.changeBonus();
		changeAltBonus();
		if(squarebonus.checkBonus())
			setAltBonus(false, false, false, false);
		System.out.println("changed Nobilitysquare's Bonus in position # " + ind);
		System.out.println();
	}
}
