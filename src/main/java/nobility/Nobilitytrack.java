package nobility;

import java.util.ArrayList;

import map.Bonus;

public class Nobilitytrack {
	private ArrayList<Nobilitysquare> track;
	
	public Nobilitytrack(){
		track = new ArrayList<>(21);
		
		for(int k=0; k<21; k++){
			Nobilitysquare tmp = new Nobilitysquare();
			track.add(k, tmp);
		}
		
		for(int k=0; k<21; k++){
			switch(k){
				case 2: track.get(2).setBonus(new Bonus(0, 2, 2, 0, false, 0)); 
						break;
				case 4: track.get(4).setAltBonus(true, false, false, false);
						break; //use bonus of a city which I already built in (except nobility points)
				case 6: track.get(6).setBonus(new Bonus(0, 0, 0, 0, true, 0)); 
						break;
				case 8: track.get(8).setBonus(new Bonus(0, 0, 3, 1, false, 0)); 
						break;
				case 10: track.get(10).setAltBonus(false, true, false, false); 
						break; //get a permittile without paying the cost
				case 12: track.get(12).setBonus(new Bonus(1, 0, 5, 0, false, 0)); 
						break;
				case 14: track.get(14).setAltBonus(false, false, true, false); 
						break; //use bonus of a permittile I already bought
				case 16: track.get(16).setAltBonus(false, false, false, true); 
						break; //use bonus of a city which I already built in (x2)
				case 18: track.get(18).setBonus(new Bonus(0, 0, 8, 0, false, 0)); 
						break;
				case 19: track.get(19).setBonus(new Bonus(0, 0, 2, 0, false, 0)); 
						break;
				case 20: track.get(20).setBonus(new Bonus(0, 0, 3, 0, false, 0)); 
						break;
				default: break;
			}
		}
	}
	
	/** 
	 * @param p
	 * @return the Nobily Square object at the p-position in the Nobility Track
	 */
	public Nobilitysquare getSquare(int p){
		return track.get(p);
	}
	
	public int getSize(){
		return track.size();
	}
	
	/**
	 * shows the bonus in every square of the Nobility Track (if present)
	 */
	public void showNobilityTrack(){
		for (int k=1; k<21; k++){
			int flag = track.get(k).getAltBonus();
			System.out.print("Square # " + k + " -> ");
			if(track.get(k).hasRealBonus())
				track.get(k).getBonus().showBonus();
			if(flag == 1)
				System.out.println("Get a free bonus of a City which you've already built an emporium in (excluded cities with Nobility points)");
			if(flag == 2)
				System.out.println("Take a face up Permit Tile without paying the cost");
			if(flag == 3)
				System.out.println("Receive the bonus of a Permit Tile that you've already bought");
			if(flag == 4)
				System.out.println("Get 2 free bonus of 2 Cities which you've already built an emporium in (excluded cities with Nobility points)");
			if(!track.get(k).hasRealBonus() && flag!=1 && flag!=2 && flag !=3 &&flag!=4)
				System.out.println();
		}
	}
}
