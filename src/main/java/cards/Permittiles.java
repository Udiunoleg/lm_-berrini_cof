package cards;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import map.Bonus;
import map.City;
import map.Region;

public class Permittiles {
	private Bonus bs;
	private boolean usable;
	private ArrayList<Integer> ediblecity;
	private Region reg;
	
	public Permittiles(Region rname){
		usable = true;
		bs = new Bonus();
		ediblecity = new ArrayList<>();
		reg = rname;
	}
	
	public Permittiles(Region rname, Bonus b){
		usable = true;
		bs = b;
		ediblecity = new ArrayList<>();
		reg = rname;
	}
	
	public Bonus getBonus(){return bs;}
	
	public Region getRegion(){return reg;}
	
	/**
	 * calls the Bonus' method setRandomBonus to add random Bonus on a Permit Tile
	 * @param n number of the city of that Tile
	 */
	public void setRandomBonusTile(int n){
		bs.setRandomBonus(n); 
	}
	
	/**
	 * sets n random cities on a Tile
	 * @param n number of cities to add on the Tile
	 */
	public void setRandomCityTile(int n){
		Random rand = new Random();
		int citynum = n;
		int choosen;
		int firstchoosen = -1; //an integer different from 0-5
		int secondchoosen = -1; //an integer different from 0-5
		for (int i=0; i<citynum; i++){
			do{
				choosen = rand.nextInt(5);
			}while(choosen == firstchoosen || choosen == secondchoosen);
			ediblecity.add(choosen);

			if(i==0){
				firstchoosen = choosen;
			}
			if(i==1){
				secondchoosen = choosen;
			}
		}
	}

	public List<Integer> getCity(){
		List<Integer> ci= new ArrayList<>();
		for(int i=0; i < ediblecity.size(); i++){
			ci.add(ediblecity.get(i));
		}
		return ci;
	}
	
	/**
	 * checks if a Permit Tile has been used or not
	 * @return true if it can be used, false if it has been already used
	 */
	public Boolean isUsable(){return  usable;}
	
	public void usePermitTile(){usable = false;}
	
	public void showEdibleCitySize(){System.out.println(ediblecity.size());}
	
	public void showCityIntTile(){
		for(int i =0; i < ediblecity.size(); i++){
			System.out.print("-" + ediblecity.get(i));
		}
		System.out.println();
	}
	
	 /**
	  * prints name of the region which the Tile belongs to, Cities and Bonus of the Permit Tile
	  */
	public void showTile(){
		System.out.println(reg.getRegionName() + "'s Permit Tile");
		System.out.print("Cities:");
		ArrayList<City> ci = reg.intToCity(ediblecity);
		for(int i =0; i< ci.size(); i++){
			System.out.print(" - ");
			System.out.print(ci.get(i).cityName());
		}
		System.out.println();
		//It shows the bonus of this permit tile
		System.out.print("Bonus: ");
		bs.showBonus();
		if(!usable){
			System.out.println("USED");
		}
		System.out.println();
	}
	
	/**
	 * call Bonus' method changeBonus and prints the region of the tile and its number inside the deck
	 * @param position number of the Tile changed in the deck
	 */
	public void pTilesChangeBonus(int position){
		bs.changeBonus();
		System.out.println("Changed " +reg.getRegionName()+ "'s Permit Tile #" + position);
		System.out.println();
	}
}
