package cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import map.Player;
import map.Region;

public class Permitdeck implements Deck {
	private ArrayList<Permittiles> pdeck;
	public ArrayList<Permittiles> shown;
	private Region reg;
	private int number;
	
	public Permitdeck(Region r, int NUM_PLAYER){
		shown = new ArrayList<>(2);
		pdeck = new ArrayList<>();
		reg = r;		
		number = NUM_PLAYER;
		createDeck(reg);
	}
	
	/**
	 * creates a Permit Deck of a selected Region
	 * @param r Region
	 */
	public void createDeck(Region r){
		reg = r;
		int i;
		int citnum;
		int flag;
		
		if(number > 4)
			flag = 5;
		else
			flag = 0;
		
		for(citnum = 1, i=0; i<5+flag; i++){
			pdeck.add(i, new Permittiles(reg));
			pdeck.get(i).setRandomBonusTile(citnum);
			pdeck.get(i).setRandomCityTile(citnum);
		}
		for(citnum = 2, i=5+flag; i<10+(flag*2); i++){
			pdeck.add(i, new Permittiles(reg));
			pdeck.get(i).setRandomBonusTile(citnum);
			pdeck.get(i).setRandomCityTile(citnum);
		}
		for(citnum = 3, i=10+(flag)*2; i<15+(flag)*3; i++){
			pdeck.add(i, new Permittiles(reg));
			pdeck.get(i).setRandomBonusTile(citnum);
			pdeck.get(i).setRandomCityTile(citnum);
		}
		
		Collections.shuffle(pdeck);
		flip();
		flip();
	}
	
	@Override
	public void shuffle(){
		Collections.shuffle(pdeck);
	}
	
	/**
	 * extract the first card from the deck and put it in shown cards' array
	 */
	public void flip(){
		Permittiles extile;
		try {
			extile = pdeck.get(0);
			shown.add(extile);
			pdeck.remove(0);
        } catch (IndexOutOfBoundsException e) {
        	Logger.getAnonymousLogger().log(Level.SEVERE, "context", e);
        }
	}
	
	/**
	 * changes the 2 visible Permit Tiles of a Region
	 */
	public void changeShown(){
		Random ra = new Random();
		Permittiles tmp;
		//It takes the second shown card and puts it back into the deck (in a random position)
		tmp = shown.get(1);
		pdeck.add(ra.nextInt(pdeck.size()), tmp);
		shown.remove(1);
		//It takes the first shown card and puts it back into the deck (in a random position)
		tmp = shown.get(0);
		pdeck.add(ra.nextInt(pdeck.size()), tmp);
		shown.remove(0);
		flip();
		flip();
	}
	
	/**
	 * adds the selected Permit Card to the hand of the Player 
	 * @param pl Player
	 * @param ind index of the selected Permit Card
	 */
	public void drawShowedTile(Player pl, int ind){
		Permittiles tile;
		tile = shown.get(ind);
		pl.addPermittile(tile);
		shown.remove(ind);
		flip();
	}
	
	@Override 
	public void draw(Player pl){
		//void method in order to implement interface Deck
	}
	
	/**
	 * discards the selected Permit Card and put it back in the Deck in a random position
	 * @param card
	 */
	public void discard(Permittiles card){
		Random ra = new Random();
		pdeck.add(ra.nextInt(pdeck.size()), card);
	}
	
	public int deckSize(){return pdeck.size();}
	
	/**
	 * @return the first Card in the Permit Deck
	 */
	public Permittiles getFirstCard(){
		Permittiles tile;
		tile = pdeck.get(0);
		return tile;
	}
	
	public Permittiles getPTile(int index){
		return pdeck.get(index);
	}
	
	/**
	 * shows the whole Permit Deck of a region in order to change the Bonus in some Permit Tiles before the game starts
	 */
	public void showPdeck(){
	 	if(number<5){
	 		for(int j=0; j<13; j++){
	 			System.out.print(j + ") ");
	 			pdeck.get(j).showTile();
	 		}	
		}else{
	 		for(int j=0; j<28; j++){
	 			System.out.print(j + ") ");
	 			pdeck.get(j).showTile();
	 		}
	 	}
	}	
}