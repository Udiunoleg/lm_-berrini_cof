package cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import map.Player;

public class Politicsdeck implements Deck{
	private ArrayList<Politicscard> polideck;
	private static final int BLACKCARDS = 13;
	private static final int LIGHTBLUECARDS = 13;
	private static final int ORANGECARDS = 13;
	private static final int WHITECARDS = 13;
	private static final int PINKCARDS = 13;
	private static final int VIOLETCARDS = 13;
	private static final int JOLLYCARDS = 12;
	
	public Politicsdeck(){
		polideck = new ArrayList<>();
		int i;
		for(i=0; i<BLACKCARDS; i++)
			polideck.add(new Politicscard(Color.BLACK));
		for(i=0; i<LIGHTBLUECARDS; i++)
			polideck.add(new Politicscard(Color.LIGHTBLUE));
		for(i=0; i<ORANGECARDS; i++)
			polideck.add(new Politicscard(Color.ORANGE));
		for(i=0; i<WHITECARDS; i++)
			polideck.add(new Politicscard(Color.WHITE));
		for(i=0; i<PINKCARDS; i++)
			polideck.add(new Politicscard(Color.PINK));
		for(i=0; i<VIOLETCARDS; i++)
			polideck.add(new Politicscard(Color.VIOLET));
		for(i=0; i<JOLLYCARDS; i++)
			polideck.add(new Politicscard(Color.JOLLY));
		
		Collections.shuffle(polideck);
	}
	
	/** shuffles the Politicsdeck
	 * @Override
	 */
	@Override
	public void shuffle(){
		Collections.shuffle(polideck);
	}
	
	/**
	 * draws a card from the Politicsdeck and add it to the Player's hand
	 *  @Override
	 */
	@Override
	public void draw(Player p){
		Politicscard excard;
		try {
			excard = polideck.get(0);
			p.addPoliticscard(excard);
    		polideck.remove(0);
        } catch (IndexOutOfBoundsException e) {
        	Logger.getAnonymousLogger().log(Level.SEVERE, "context", e);
          }
	}
	
	/**
	 * discards the card given as a parameter and re-adds it to the Politicsdeck
	 * @param card
	 */
	public void discard(Politicscard card){
		Random ra = new Random();
		polideck.add(ra.nextInt(polideck.size()), card);
	}
	
	//FOR TESTS ONLY
	public ArrayList<Politicscard> getArray(){return polideck;}
	public int getSize(){return polideck.size();}
}
