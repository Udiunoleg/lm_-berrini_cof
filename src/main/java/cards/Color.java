package cards;

public enum Color {
	BLACK ("Black", 0), LIGHTBLUE("LightBlue", 1), ORANGE("Orange", 2),
	PINK("Pink", 3), VIOLET("Violet", 4), WHITE("White", 5), 
	JOLLY("Jolly", 9), YELLOW("Yellow", 6), 
	GRAY("Gray", 7);
	private String abbreviation;
	private int numcol;
	
	private Color(String abbreviation, int pos){
		this.numcol= pos;
		this.abbreviation = abbreviation;
	}
	public String getAbbreviation(){
		return abbreviation;
	}
	public static Color getColor(int num){
		Color[] colors ={Color.BLACK, Color.LIGHTBLUE, Color.ORANGE, Color.PINK, Color.VIOLET, Color.WHITE, Color.YELLOW, Color.GRAY};
		if(-1<num&& num<8)
			return colors[num];
		else
			return Color.JOLLY;
	}
}