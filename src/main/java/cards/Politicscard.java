package cards;

public class Politicscard {
	private Color color;
	
	public Politicscard(Color color){
		this.color= color;
	}

	public Color getColor() {return color;}

	public void setColor(Color color) {this.color = color;}
	
	public void showCard(){
		System.out.print(color.getAbbreviation() + " - ");
	}
	
}