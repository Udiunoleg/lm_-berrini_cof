package junit;

import static org.junit.Assert.*;
import org.junit.Test;

import cards.Color;
import controllers.PlayerController;
import controllers.Market;
import map.Constants;
import map.Map;

public class PlayerControllerCreationTest {

	@Test
	public void test() {
		Constants cos = new Constants();
		Map m = new Map("SEA A", "HILL A", "MOUNTAIN A", cos);
		System.out.println(Color.BLACK.getAbbreviation());
		Market mark = new Market();
//		PlayerController p = new PlayerController(Color.BLACK, m, cos, mark );
		PlayerController p = new PlayerController(Color.getColor(1), m, cos, mark );
		System.out.println(p.getPlayer().getPlayerColor().getAbbreviation());
		assertNotNull(p);
		p.playerInitialize(5, 10);
		System.out.println(p.getPlayer().getAssistants());
		System.out.println(p.getPlayer().getCoins());
		p.getPlayer().showPCard();
		p.drawPolicard();
		p.getPlayer().showPCard();
//		p.electCouncillor();
//		p.selectAction();
		p.showPermitTiles();
	}

}
