package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import map.Constants;
import map.Map;
import map.Region;

public class RegionTest {

    /**
     * tests that Region exists
     * Hellar is in the Hill Region
     * shows all the cities and their respective casual bonus 
     */
	@Test
	public void test() {
		Constants cos = new Constants();
		Map m = new Map("SEA A", "HILL A", "MOUNTAIN A", cos);
		Region r = m.getHillR();
		assertNotNull(r);
		
		System.out.println(r.getACity(0).cityName());
		for(int i =0 ; i < r.getAllCity().size(); i++){
			System.out.println(r.getAllCity().get(i).cityName());
		}
		
		System.out.println(r.getCityByName("Hellar").cityName());
		assertTrue(r.cityIsThere("Hellar"));
		System.out.println("-----------------------");
		r.showAllRegionCity();
		r.showAllCityBonusRegion();
	}

}
