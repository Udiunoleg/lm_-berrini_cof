package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import cards.Color;
import cards.Permitdeck;
import cards.Permittiles;
import map.Constants;
import map.Map;
import map.Player;
import map.Region;

public class RegionTilesTest {

	@Test
	public void test() {
		Constants cons = new Constants();
		Map m = new Map("SEA A", "HILL A", "MOUNTAIN A", cons);
		Permitdeck per = new Permitdeck(m.getMountR(), 5);
		m.getSeaR().getRegionName();
		m.getHillR().getRegionName();
		m.getMountR().getRegionName();
		assertNotNull(m.getHillR().getPDeck().deckSize());
		m.getMountR().showPermitTile(m.getMountR().getPDeck().getFirstCard());	
		m.getMountR().showShownPermitTiles();
		m.getMountR().getPDeck().changeShown();
		Permittiles card = new Permittiles(m.getSeaR());
		card.setRandomCityTile(3);
		card.setRandomBonusTile(3);
		card.showTile();
		System.out.println(per.deckSize());
//		per.createDeck(m.getMountR());
	}

}
