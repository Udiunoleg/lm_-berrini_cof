package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import map.Constants;
import map.King;
import map.Map;

/**
 * tests that the King is created, 
 * is in Juvelar at the beginning of the game,
 * shows correctly the names of the cities surrounding Juvelar;
 * shows that he properly moves to Lyram and the cities which are linked to this city
 */
public class KingTest {

	@Test
	public void test() {
		Constants co = new Constants();
		Map m = new Map("SEA B", "HILL B", "MOUNTAIN A", co);
		assertNotNull(m.getKing());
		King k = m.getKing();
		k.showKingCouncil();
		k.showWhereIs();
		k.showNearCityNames();
		k.move(m.nameToCity("Lyram"));
		k.showWhereIs();
		k.showNearCityNames();
	}
}
