package junit;

/**
 * tests addCouncillor methods, (the first insert a White one in first position,
 * the second a Violet one in first position)
 *  and payForCouncil
 */
import static org.junit.Assert.*;

import org.junit.Test;

import cards.Color;
import council.Council;
import council.Councillor;
import council.CouncillorPool;

public class CouncilTest {

	@Test
	public void test() {
		CouncillorPool pool = new CouncillorPool();
		Council coun = new Council(pool);
		Councillor c = new Councillor(Color.VIOLET);
//		King kg = new King();
		Color col = Color.WHITE;
		coun.addCouncillor(col);
		coun.addCouncillor(c);
		coun.getInfo();
		coun.printOrder();
		
		assertEquals(8, coun.payForCouncil(2, 1));
	}
}
