package junit;

import static org.junit.Assert.*;


import map.BonusCityPool;

public class BonusCityPoolShuffleTest {

	/**creates 2 separate deck of bonus city and assert that the 2 arrays are not the same
	 * @Test
	 */
	public void test() {
		BonusCityPool deck1 = new BonusCityPool();
		BonusCityPool deck2 = new BonusCityPool();
		 assertEquals(deck1.getArray(), deck1.getArray());
		 assertNotEquals(deck1.getArray(), deck2.getArray());
		 deck1.removeFirstBonus();
		 deck1.printAllBonus();
	}

}
