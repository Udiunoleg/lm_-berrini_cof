package junit;

import static org.junit.Assert.*;
import org.junit.Test;

import cards.Color;
import cards.Politicscard;
import cards.Politicsdeck;

public class PoliticsdeckShuffleTest {

	@Test
	public void test() {
		Politicsdeck deck1 = new Politicsdeck();
		Politicsdeck deck2 = new Politicsdeck();
		Politicscard card = new Politicscard(Color.JOLLY);
		assertEquals(deck1.getArray(), deck1.getArray());
		deck1.shuffle();
		assertNotEquals(deck1.getArray(), deck2.getArray());
		deck1.discard(card);
	}

}
