package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import map.Constants;
import map.Map;

public class RegionCreationTest {

	@Test
	public void test() {
		Constants cos = new Constants();
		Map m = new Map("SEA A", "HILL A", "MOUNTAIN A", cos);
		System.out.println(m.getSeaR().getRegionName());
		assertNotNull(m.getSeaR());
		System.out.println(m.getHillR().getRegionName());
		assertNotNull(m.getHillR());
		System.out.println(m.getMountR().getRegionName());	
		assertNotNull(m.getMountR());
		assertNotNull(m.getHillR().getCouncil());
		assertNotNull(m.getMountR().getCouncil());
		assertNotNull(m.getSeaR().getPDeck());
		assertNotNull(m.getMountR().getAllCity());
		assertNotNull(m.getSeaR().getRewardBonus());
		m.getSeaR().getRewardBonus().showBonus();
		}

}
