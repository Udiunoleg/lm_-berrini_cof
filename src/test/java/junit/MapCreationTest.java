package junit;

import static org.junit.Assert.*;
import org.junit.Test;

import cards.Permittiles;
import map.Bonus;
import map.Constants;
import map.Map;


public class MapCreationTest {
	
	/**
	 * tests that:
	 * Map is created, 
	 * Regions exist
	 * Nobilitytrack exists
	 * the King is in Juvelar, then is moved to Jellar
	 * shows correctly the names and Bonus of the cities
	 * there is a King Bonus of 25 Victory Points
	 */
	@Test
	public void test() {
		Constants co = new Constants();
		Map m = new Map("SEA A", "HILL A", "MOUNTAIN A", co);
		assertNotNull(m);
		
		assertNotNull(m.getSeaR());
		System.out.println(m.getSeaR().getRegionName());
		assertNotNull(m.getMountR());
		System.out.println(m.getHillR().getRegionName());
		assertNotNull(m.getHillR());
		System.out.println(m.getMountR().getRegionName());
		assertNotNull(m.getRegion("hill"));
		System.out.println(m.getRegion("hill").getRegionName());
		
		assertNotNull(m.getNobilitytrack());
		System.out.println("Nobility Track Size: " + m.getNobilitytrack().getSize()); 
		m.getNobilitytrack().getSquare(2).getBonus().showBonus();
		System.out.println("Alt BONUS " + m.getNobilitytrack().getSquare(4).getAltBonus());
		
		System.out.println("The King is in " + m.getKing().whereIs().cityName() + " City");
		m.getKing().getKingCouncil();
		m.getKing().showKingCouncil();
		m.getKing().showNearCityNames();
		m.getKing().move(m.nameToCity("Hellar"));
		System.out.println("The King is in " + m.getKing().whereIs().cityName() + " City");
		m.getKing().showNearCityNames();
		m.showAllCityName();
		m.showAllCityBonus();
		
		assertTrue(m.hasKingBonus());
		Bonus bs = new Bonus();
		bs = m.getKingbonus();
		assertEquals(25, bs.getBVPointNumber());
		
		m.showAllCouncil();
		m.showAllShownPermitTiles();
		Permittiles pert1 = new Permittiles(m.getHillR());
		m.showCityPermitTile(pert1);
		m.showPermitTile(pert1);
		
		m.setRandomEmpo();
	}

}
