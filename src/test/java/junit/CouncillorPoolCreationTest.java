package junit;

import static org.junit.Assert.*;

import cards.Color;
import council.CouncillorPool;

	/** checks that the Pool exists, 
	 * a Councillor Black is present in it at the moment of creation
	 * and the size after 2 extractions is 22
	 * @Test
	 */
public class CouncillorPoolCreationTest {

	public void test() {
		Color col = Color.BLACK;
		boolean bol;
		CouncillorPool pool = new CouncillorPool();
		assertNotNull(pool);
		bol = pool.isFound(col);
		assertTrue(bol);
		pool.showAvColors();
//		pool.drawCouncillor();
//		pool.drawCouncillor();
//		pool.drawCouncillor();
//		pool.drawCouncillor();
		pool.drawCouncillor(Color.LIGHTBLUE);
		pool.drawCouncillor(Color.LIGHTBLUE);
		assertEquals(22, pool.getSize());
	}

}
