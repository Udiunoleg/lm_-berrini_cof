package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import nobility.Nobilitytrack;

public class NobilitytrackSizeTest {

	@Test
	public void test() {
		Nobilitytrack circuito = new Nobilitytrack();
		int dim = circuito.getSize();
		assertEquals(dim, 21);
	}

}
