package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import map.Bonus;
import nobility.Nobilitytrack;

public class NobilitytrackBonusTest {

	/**
	 * compares the Bonus of default Nobilitytrack with some new Bonus
	 */
	@Test
	public void test() {
		Nobilitytrack nob = new Nobilitytrack();
		nob.showNobilityTrack();
		Bonus bon;
		Bonus nb;
		int i=2;
		int num1;
		int num2;
		int num3;
		int num4;
		
		bon = nob.getSquare(i).getBonus();
		bon.showBonus();
		nb = new Bonus(0, 2, 2, 0, false, 0);
		num1 = bon.getBCoinNumber();
		num2 =  nb.getBCoinNumber();
		num3 = bon.getBNobilityNumber();
		num4 = 2;
		//nob.getSquare(7).nobilityChangeBonus(7);
		nb.showBonus();
		assertEquals(num1, num2);
		assertNotEquals(num3, num4);
		
	}

}
