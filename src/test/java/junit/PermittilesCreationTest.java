package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import cards.Permittiles;
import council.CouncillorPool;
import map.Bonus;
import map.Constants;
import map.Region;


public class PermittilesCreationTest {
	
/**
 * tests the creation of 3 Permit Tiles and the correct number of Bonus' set
 */
	@Test
	public void test() {
		CouncillorPool pool = new CouncillorPool();
		Constants co = new Constants();
		Region reg = new Region("sea", pool, co);
		Bonus b = new Bonus(1,3,0,0,false,0);
		
		Permittiles perm = new Permittiles(reg, b);
		assertTrue(perm.getBonus().notNobilityBonus());
		assertEquals(perm.getBonus().getBAssistantNumber(), 1);
		perm.getBonus().setMainAction(0);
		assertTrue(perm.getBonus().getBMainaction());
		
		Permittiles perm2 = new Permittiles(reg);
		perm2.getBonus().setAssistants(2);
		perm2.getBonus().setCoins(1);
		perm2.getBonus().setNobility(2);
		perm2.getBonus().setPolicard(5);
		perm2.getBonus().setVPoint(2);
		assertEquals(perm2.getBonus().getBVPointNumber(), 2);
		assertEquals(perm2.getBonus().getBPolicardNumber(), 5);
		assertTrue(perm2.getBonus().checkBonus());
		
		assertNotEquals(perm, perm2);
		
		Permittiles perm3 = new Permittiles(reg);
		perm3.setRandomBonusTile(3);
		perm3.setRandomCityTile(3);

	}

}
