package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import cards.Color;
import cards.Permitdeck;
import cards.Politicsdeck;
import map.Constants;
import map.Map;
import map.Player;

/**
 * tests that, after creating a Map, Politicsdeck and Permitdeck exist,
 * they are not null and the size of Politicsdeck is just like that we expected
 *
 */
public class MapCreationDeckTest {

	
	@Test
	public void test() {
		Constants co = new Constants();
		Map ma = new Map("SEA B", "HILL A", "MOUNTAIN B", co);
		Player p1 = new Player(10, Color.BLACK);
		Politicsdeck pdeck;
		Permitdeck pd;
		pd = ma.getHillR().getPDeck();
		pdeck = ma.getPolideck();
		assertNotNull(pdeck);
		assertNotNull(pd);
		
		pdeck.draw(p1);
		pdeck.draw(p1);
		pdeck.draw(p1);
		assertEquals(87, pdeck.getSize());

//		p1.showPCard();
//		pdeck.shuffle();
	}

}
