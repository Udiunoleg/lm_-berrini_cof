package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import nobility.Nobilitytrack;

public class NobilitytrackAltBonusTest {

	/**
	 * tests the presence of Alternative Bonus on the default Nobilitytrack's squares
	 */
	@Test
	public void test() {
		Nobilitytrack n1 = new Nobilitytrack();
		int i=16;
		int j=4;
		int flag1 = n1.getSquare(i).getAltBonus();
		int flag2 = n1.getSquare(j).getAltBonus();
		assertEquals(4, flag1);		
		assertNotEquals(3, flag2);
		
		n1.getSquare(2).getBonus().showBonus();
		System.out.println("Alt BONUS " + n1.getSquare(8).getAltBonus());
	}

}
