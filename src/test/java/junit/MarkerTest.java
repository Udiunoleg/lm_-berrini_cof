package junit;

import static org.junit.Assert.*;

import org.junit.Test;
import cards.Color;
import map.Constants;
import map.Map;
import map.Player;
import nobility.Marker;
import nobility.Nobilitytrack;

public class MarkerTest {

	@Test
	public void test() {
		Player pl = new Player(10, Color.ORANGE);
		Constants co = new Constants();
		Map mp = new Map("SEA A", "HILL A", "MOUNTAIN A", co);
		Nobilitytrack nt = mp.getNobilitytrack();
		Marker mar = new Marker(pl, mp, nt);
		mar.move(2);
		assertEquals(2, pl.getCoins());
		assertEquals(2, pl.getVpoints());
		//mar.move(1);
		//System.out.println(mar.getPosition());
	}

}
